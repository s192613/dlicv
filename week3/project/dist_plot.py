import torch
import argparse
import matplotlib.pyplot as plt
import seaborn as sns
import os

import week3.project.helper as helper
from week3.project.main import normalize

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
plt.style.use('ggplot')

PLOT_CONTEXT = {
    'text.usetex': True,
    'font.size': 12,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
    'axes.formatter.useoffset': False,
    "text.latex.preamble": [r'\usepackage{cmbright}']
}


def parse_cmd():
    parser = argparse.ArgumentParser(description="script to generate the FID plots.")
    parser.add_argument('--data_path', type=str, default='data/horse2zebra',
                        help='path to the data directory (default: data/horse2zebra)')
    parser.add_argument('--log_path', type=str, default="logs/cycle-gan-v023",
                        help='path to directory containing the pickled images.')

    args = parser.parse_args()

    return args


# Load checkpoint, eval(), desactivate gradients predicts for A->B + real B, B->A + real A


if __name__ == '__main__':
    args = parse_cmd()
    data = helper.prepare_data(args.data_path, batch_size=32)
    nets, _ = helper.load_checkpoint(args.log_path, epoch=-1, device=device)

    for net in nets:
        nets[net].eval()

    a2b_fake_scores = []
    b_real_scores = []
    b2a_fake_scores = []
    a_real_scores = []

    with torch.no_grad():
        for i, (a_images, b_images) in enumerate(data["test"][1]):
            n = a_images.shape[0]

            a_images, b_images = normalize(a_images.to(device)), normalize(b_images.to(device))
            # A->B
            a2b_fake_score = nets["discr_b"](nets["gen_ab"](a_images))
            b_real_score = nets["discr_b"](b_images)

            a2b_fake_scores.extend(a2b_fake_score.view(a2b_fake_score.size(0), -1).mean(1).cpu().tolist())
            b_real_scores.extend(b_real_score.view(b_real_score.size(0), -1).mean(1).cpu().tolist())

            # B->A
            b2a_fake_score = nets["discr_a"](nets["gen_ba"](a_images))
            a_real_score = nets["discr_a"](a_images)

            b2a_fake_scores.extend(b2a_fake_score.view(b2a_fake_score.size(0), -1).mean(1).cpu().tolist())
            a_real_scores.extend(a_real_score.view(a_real_score.size(0), -1).mean(1).cpu().tolist())

    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(10, 4))

    # a2b

    sns.distplot(a2b_fake_scores, ax=axs[0], kde=False, bins=10)
    sns.distplot(b_real_scores, ax=axs[0], kde=False, bins=10)
    axs[0].legend(['Fake Zebras', 'True Zebras'])
    axs[0].set_title('Horse-to-Zebra')
    axs[0].set_xlabel('Score')

    # b2a
    sns.distplot(b2a_fake_scores, ax=axs[1], kde=False, bins=10)
    sns.distplot(a_real_scores, ax=axs[1], kde=False, bins=10)
    axs[1].legend(['Fake Horses', 'True Horses'])
    axs[1].set_title('Zebra-to-Horse')
    axs[1].set_xlabel('Score')

    plt.tight_layout()
    plt.savefig(os.path.join(args.log_path, 'scores_distr.pdf'))
