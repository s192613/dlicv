import torch
import tqdm
import numpy as np
import os
import argparse
import pickle
from torch.utils.tensorboard import SummaryWriter

import week3.project.helper as helper
from week3.project.image_pool import ImagePool

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def parse_cmd():
    parser = argparse.ArgumentParser(description="script to train segmentation model for the horse-to-zebra task.")
    parser.add_argument('--model_name', type=str, default='cycle-gan',
                        help='name given to the mode (default: cycle-gan)')
    parser.add_argument('--gen_loss', type=str, choices=('lsgan', 'wgan', 'bce'), default='lsgan',
                        help='loss used to compare predictions (default: lsgan)')
    parser.add_argument('--img_loss', type=str, choices=('l1', 'l2'), default='l1',
                        help='loss used to compare images  (default: l1)')
    parser.add_argument('--batch_size', type=int, default=1, help='input batch size for training (default: 1)')
    parser.add_argument('--num_epochs', type=int, default=50, help='number of training epochs (default: 50)')
    parser.add_argument('--optimizer', type=str, default='adam', choices=('sgd', 'adam', 'rmsprop'),
                        help='optimizer to be used (default:adam)')
    parser.add_argument('--lr_discr', type=float, default=2e-4, help='learning rate of discriminators (default: 2e-4)')
    parser.add_argument('--lr_gen', type=float, default=2e-4, help='learning rate of generators (default: 2e-4)')
    parser.add_argument('--dropout_rate', type=float, default=.0,
                        help='dropout to be applied in the network (default: 0.0)')
    parser.add_argument('--resnet_blocks', type=int, default=6,
                        help='number of resnet blocks to include in the Generator (default:6)')
    parser.add_argument('--logdir', type=str, default='logs',
                        help='directory where the logs will be saved (default: logs/)')
    parser.add_argument('--save_every', type=int, default=5,
                        help='frequency at which the network should be saved (default: 5)')
    parser.add_argument('--data_path', type=str, default='data/horse2zebra',
                        help='location of the dataset (default: data/horse2zebra/)')
    parser.add_argument('--cycle_lambda', type=float, default=10, help='weighting of cycle loss (default: 10)')
    parser.add_argument('--ident_lambda', type=float, default=5, help='weighting of identity loss (default: 5)')
    parser.add_argument('--gen_buffer_size', type=int, default=0,
                        help='Buffer size of gen images to feed discriminator from (default: 0)')

    args = parser.parse_args()

    return args


def normalize(in_tens):
    return in_tens * 2. - 1.


def unnormalize(in_tens):
    return (in_tens + 1.0) / 2.0


def set_requires_grad(net, requires_grad=False):
    if net is not None:
        for param in net.parameters():
            param.requires_grad = requires_grad


def get_real_fake_ratio(d_out):
    p = torch.sigmoid(d_out)
    p_thresh = torch.where(p >= 0.5, torch.tensor([1.0]).to(device), torch.tensor([0.0]).to(device))
    num_real = torch.sum(p_thresh)
    num_fake = p.shape[2] * p.shape[3] - num_real
    real_fake_ratio = num_real.double() / num_fake.double()
    return real_fake_ratio


def train(models, gan_loss_fun, img_loss_fun, data, optimizers, writer, log_path, num_epochs, save_every, cycle_lambda,
          ident_lambda, pool_size):
    folds = list(data.keys())
    for model in models.values():
        model.to(device)

    fake_pool_a = ImagePool(pool_size)
    fake_pool_b = ImagePool(pool_size)

    for epoch in tqdm.tqdm(range(num_epochs), unit='epoch'):
        print(f"\n------------- EPOCH {epoch}/{num_epochs} ---------------")
        losses = {"gen_ab": {fold: [] for fold in folds},
                  "gen_ba": {fold: [] for fold in folds},
                  "discr_a": {fold: [] for fold in folds},
                  "discr_b": {fold: [] for fold in folds}}

        images = {"A2B": {"Original": [], "Fake": [], "Reconstructed": [], "Identity": []},
                  "B2A": {"Original": [], "Fake": [], "Reconstructed": [], "Identity": []}}

        for fold in tqdm.tqdm(folds, total=len(folds), unit='fold'):
            dset, loader = data[fold]
            torch.set_grad_enabled(fold not in ("test"))
            num_img_dataset = len(dset)

            # Setting model in training or eval mode
            for model in models.values():
                model.train(fold not in ("test"))

            for minibatch_no, (a_real, b_real) in tqdm.tqdm(enumerate(loader), total=len(loader), unit='batch'):
                a_real, b_real = normalize(a_real.to(device)), normalize(b_real.to(device))

                ### Generator
                b_fake = models["gen_ab"](a_real)
                a_rec = models["gen_ba"](b_fake)
                a_iden = models["gen_ba"](a_real)
                a_fake = models["gen_ba"](b_real)
                b_rec = models["gen_ab"](a_fake)
                b_iden = models["gen_ab"](b_real)

                # Not necessary during testing
                if fold == "train":
                    # Discriminators require no grad
                    set_requires_grad(models["discr_a"], False)
                    set_requires_grad(models["discr_b"], False)

                    # Generator predictions
                    b_fake_pred = models["discr_b"](b_fake)
                    a_fake_pred = models["discr_a"](a_fake)

                    # Update Generator A --> B
                    gan_loss_ab = gan_loss_fun(b_fake_pred, torch.ones(b_fake_pred.shape).to(device))
                    identity_loss_b = img_loss_fun(b_iden, b_real)
                    cycle_consist_loss_a = img_loss_fun(a_rec, a_real)
                    gen_ab_loss = gan_loss_ab + ident_lambda * identity_loss_b + cycle_lambda * cycle_consist_loss_a
                    if fold == "train":
                        optimizers["gen"].zero_grad()
                        gen_ab_loss.backward()

                    # Update Generator B --> A
                    gan_loss_ba = gan_loss_fun(a_fake_pred, torch.ones(a_fake_pred.shape).to(device))
                    identity_loss_a = img_loss_fun(a_iden, a_real)
                    cycle_consist_loss_b = img_loss_fun(b_rec, b_real)
                    gen_ba_loss = gan_loss_ba + ident_lambda * identity_loss_a + cycle_lambda * cycle_consist_loss_b
                    if fold == "train":
                        gen_ba_loss.backward()
                        optimizers["gen"].step()

                    # Discriminators require grad now
                    set_requires_grad(models["discr_a"], True)
                    set_requires_grad(models["discr_b"], True)

                    # Discriminator predictions
                    b_fake = fake_pool_b.query(b_fake.detach())
                    b_fake_pred = models["discr_b"](b_fake)
                    b_real_pred = models["discr_b"](b_real)
                    a_fake = fake_pool_a.query(a_fake.detach())
                    a_fake_pred = models["discr_a"](a_fake)
                    a_real_pred = models["discr_a"](a_real)

                    # Update Discriminator A
                    discr_loss_a_real = gan_loss_fun(a_real_pred, torch.ones(a_real_pred.shape).to(device))
                    discr_loss_a_fake = gan_loss_fun(a_fake_pred, torch.zeros(a_fake_pred.shape).to(device))
                    discr_loss_a_sum = (discr_loss_a_real + discr_loss_a_fake) * 0.5  # 0.5 coming from orig repo
                    if fold == "train":
                        optimizers["discr"].zero_grad()
                        discr_loss_a_sum.backward()

                    # Update Discriminator B
                    discr_loss_b_real = gan_loss_fun(b_real_pred, torch.ones(b_real_pred.shape).to(device))
                    discr_loss_b_fake = gan_loss_fun(b_fake_pred, torch.zeros(b_fake_pred.shape).to(device))
                    discr_loss_b_sum = (discr_loss_b_real + discr_loss_b_fake) * 0.5  # 0.5 coming from orig repo
                    if fold == "train":
                        discr_loss_b_sum.backward()
                        optimizers["discr"].step()

                    # Logging losses
                    losses["gen_ab"][fold].append(gen_ab_loss.cpu().item())
                    losses["gen_ba"][fold].append(gen_ba_loss.cpu().item())
                    losses["discr_a"][fold].append(discr_loss_a_sum.cpu().item())
                    losses["discr_b"][fold].append(discr_loss_b_sum.cpu().item())

                    # Logging probabilities
                    writer.add_scalar(f'real_fake_ratio/a', get_real_fake_ratio(a_fake_pred),
                                      (epoch * num_img_dataset) + minibatch_no)
                    writer.add_scalar(f'real_fake_ratio/b', get_real_fake_ratio(b_fake_pred),
                                      (epoch * num_img_dataset) + minibatch_no)

                # Logging images
                if fold == "test":
                    if epoch == 0:
                        images["A2B"]["Original"].append(unnormalize(a_real).cpu().numpy())
                        images["B2A"]["Original"].append(unnormalize(b_real).cpu().numpy())

                    images["A2B"]["Fake"].append(unnormalize(b_fake).cpu().numpy())
                    images["A2B"]["Reconstructed"].append(unnormalize(a_rec).cpu().numpy())
                    images["A2B"]["Identity"].append(unnormalize(a_iden).cpu().numpy())
                    images["B2A"]["Fake"].append(unnormalize(a_fake).cpu().numpy())
                    images["B2A"]["Reconstructed"].append(unnormalize(b_rec).cpu().numpy())
                    images["B2A"]["Identity"].append(unnormalize(b_iden).cpu().numpy())

        # tensorboard logging: losses
        for net_key, net_val in losses.items():
            avg_loss = np.mean(net_val["train"])
            writer.add_scalar(f'loss/{net_key}/train', avg_loss, global_step=epoch)
            print(f"loss/{net_key}/train: {avg_loss:.3f}", end='\t')
        print()

        # tensorboard logging: images
        #if epoch == 0:
        #    writer.add_images('1_original/horses', np.concatenate(images["A2B"]["Original"]), epoch)
        #    writer.add_images('1_original/zebras', np.concatenate(images["B2A"]["Original"]), epoch)

        #writer.add_images('2_fake/horses', np.concatenate(images["A2B"]["Fake"]), epoch)
        #writer.add_images('2_fake/zebras', np.concatenate(images["B2A"]["Fake"]), epoch)
        #writer.add_images('3_rec/horses', np.concatenate(images["A2B"]["Reconstructed"]), epoch)
        #writer.add_images('3_rec/zebras', np.concatenate(images["B2A"]["Reconstructed"]), epoch)
        #writer.add_images('4_iden/horses', np.concatenate(images["A2B"]["Identity"]), epoch)
        #writer.add_images('4_iden/zebras', np.concatenate(images["B2A"]["Identity"]), epoch)

        # Save
        if (epoch + 1) % save_every == 0:
            # save the network
            torch.save({"gen_ab_state_dict": models["gen_ab"].state_dict(),
                        "gen_ba_state_dict": models["gen_ba"].state_dict(),
                        "gen_opt_state_dict": optimizers["gen"].state_dict(),
                        "discr_a_state_dict": models["discr_a"].state_dict(),
                        "discr_b_state_dict": models["discr_b"].state_dict(),
                        "discr_pt_state_dict": optimizers["discr"].state_dict()},
                       os.path.join(log_path, f"checkpoint_{epoch}.tar"))
            # pickle the images
            for name in ['A2B', 'B2A']:
                with open(os.path.join(log_path, f"images{name}_{epoch}.pckle"), 'wb') as f:
                    pickle.dump(images[name], f)


if __name__ == '__main__':
    args = parse_cmd()

    # Parse args
    data = helper.prepare_data(args.data_path, args.batch_size)
    gen_loss_fun = helper.get_gan_loss_fun(args.gen_loss)
    img_loss_fun = helper.get_img_loss_fun(args.img_loss)
    nets = helper.get_networks(dropout_rate=args.dropout_rate, resnet_blocks=args.resnet_blocks)
    opts = helper.get_optimizers(nets, args.optimizer, args.lr_gen, args.lr_discr)

    # Logging
    net_version = helper.get_version(name=args.model_name, logdir=args.logdir)
    log_path = os.path.join(args.logdir, args.model_name + f"-v{net_version}")
    writer = SummaryWriter(log_path)
    helper.save_config(log_path, args)

    # Train
    train(nets, gen_loss_fun, img_loss_fun, data, opts, writer, log_path, args.num_epochs, args.save_every,
          args.cycle_lambda,
          args.ident_lambda,
          args.gen_buffer_size)
