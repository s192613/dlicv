import os
import argparse
import torch
import torch.nn as nn
from torchvision import models as models
import numpy as np
import scipy
import pickle
from torchvision.transforms import transforms
import torch.nn.functional as F
import matplotlib.pyplot as plt
import matplotlib as mpl
import json

from week3.project.cycle_dataset import CycleDataset

plt.style.use('ggplot')

PLOT_CONTEXT = {
    'text.usetex': True,
    'font.size': 12,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
    'axes.formatter.useoffset': False,
    "text.latex.preamble": [r'\usepackage{cmbright}']
}

KEYS = ['Fake', 'Reconstructed', 'Identity']
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

pi_net = None


def parse_cmd():
    parser = argparse.ArgumentParser(description="script to generate the FID plots.")
    parser.add_argument('--data_path', type=str, default='data/horse2zebra',
                        help='path to the data directory (default: data/horse2zebra)')
    parser.add_argument('--log_path', type=str, default="logs/cycle-gan-v023",
                        help='path to directory containing the pickled images.')

    args = parser.parse_args()

    return args


class PartialInceptionNetwork(nn.Module):
    output_dim = 2048

    # class highly inspired from https://github.com/hukkelas/pytorch-frechet-inception-distance/blob/master/fid.py
    def __init__(self, transform_input=True):
        super().__init__()
        self.inception_network = models.inception_v3(pretrained=True, progress=True)
        self.inception_network.Mixed_7c.register_forward_hook(self.output_hook)
        self.transform_input = transform_input

    def output_hook(self, module, input, output):
        # N x 2048 x 8 x 8
        self.mixed_7c_output = output

    def forward(self, x):
        """
        Args:
            x: shape (N, 3, 299, 299) dtype: torch.float32 in range 0-1
        Returns:
            inception activations: torch.tensor, shape: (N, 2048), dtype: torch.float32
        """
        assert x.shape[1:] == (3, 299, 299), "Expected input shape to be: (N,3,299,299)" + \
                                             ", but got {}".format(x.shape)
        x = x * 2 - 1  # Normalize to [-1, 1]

        # Trigger output hook
        self.inception_network(x)

        # Output: N x 2048 x 1 x 1
        activations = self.mixed_7c_output
        activations = torch.nn.functional.adaptive_avg_pool2d(activations, (1, 1))
        activations = activations.view(x.shape[0], 2048)
        return activations


def get_inception_statistics(images, batch_size=32):
    N = images.shape[0]

    global pi_net
    if not pi_net:
        pi_net = PartialInceptionNetwork()
        pi_net.to(device)
        pi_net.eval()

    activations = np.zeros((N, pi_net.output_dim))

    with torch.no_grad():
        for start in range(0, N, batch_size):
            end = min(start + batch_size, N)
            batch = images[start:end, ...].to(device)
            out = pi_net(batch)
            activations[start:end, :] = out.cpu().numpy()

    return np.mean(activations, axis=0), np.cov(activations, rowvar=False)  # variables are along the rows


def frechet_distance(mean1, cov1, mean2, cov2):
    assert mean1.shape == mean2.shape and cov1.shape == cov2.shape

    diff = mean1 - mean2
    tr_covmean = np.trace(scipy.linalg.sqrtm(np.matmul(cov1, cov2)))
    # NOTE: sum of traces = trace of sum
    return np.dot(diff, diff) + np.trace(cov1) + np.trace(cov2) - 2 * tr_covmean


def get_generated_images(log_path):
    f = lambda fname: int(os.path.split(fname)[1][:-6].split('_')[1])
    A2B_files = sorted(list(filter(lambda fname: "imagesA2B_" in fname,
                                   [os.path.join(log_path, f) for f in os.listdir(log_path)])), key=f)
    B2A_files = sorted(list(filter(lambda fname: "imagesB2A_" in fname,
                                   [os.path.join(log_path, f) for f in os.listdir(log_path)])), key=f)

    return A2B_files, B2A_files


def get_synthetic_statistics(log_path):
    A2B_files, B2A_files = get_generated_images(log_path)
    assert len(A2B_files) == len(B2A_files)

    epochs = []
    a2b_means = {key: [] for key in KEYS}
    a2b_covs = {key: [] for key in KEYS}

    b2a_means = {key: [] for key in KEYS}
    b2a_covs = {key: [] for key in KEYS}

    for a2b_f, b2a_f in zip(A2B_files, B2A_files):
        epoch_a2b = os.path.split(a2b_f)[1][:-6].split('_')[1]
        epoch_b2a = os.path.split(b2a_f)[1][:-6].split('_')[1]
        assert epoch_a2b == epoch_b2a, f"{epoch_a2b} vs {epoch_b2a}"

        epochs.append(int(epoch_a2b))
        a2b = pickle.load(open(a2b_f, 'rb'))
        b2a = pickle.load(open(b2a_f, 'rb'))

        assert all(key in a2b.keys() for key in KEYS) == all(key in b2a.keys() for key in KEYS)

        for key in KEYS:

            if key == 'Fake':
                a2b_images = F.interpolate(torch.from_numpy(np.vstack(b2a[key])), (299, 299))
                b2a_images = F.interpolate(torch.from_numpy(np.vstack(a2b[key])), (299, 299))
            else:
                a2b_images = F.interpolate(torch.from_numpy(np.vstack(a2b[key])), (299, 299))
                b2a_images = F.interpolate(torch.from_numpy(np.vstack(b2a[key])), (299, 299))

            mean_a2b, cov_a2b = get_inception_statistics(a2b_images)
            a2b_means[key].append(mean_a2b)
            a2b_covs[key].append(cov_a2b)

            mean_b2a, cov_b2a = get_inception_statistics(b2a_images)
            b2a_means[key].append(mean_b2a)
            b2a_covs[key].append(cov_b2a)

    return epochs, (a2b_means, a2b_covs), (b2a_means, b2a_covs)


def get_real_images(data_path="data/horse2zebra", size=299):
    transform = transforms.Compose([transforms.Resize((size, size)), transforms.ToTensor()])
    dataset = CycleDataset(train=False, transform=transform, data_path=data_path)
    imagesA, imagesB = zip(*[dataset[i] for i in range(len(dataset))])

    imagesA = torch.stack(imagesA, dim=0)
    imagesB = torch.stack(imagesB, dim=0)

    return imagesA, imagesB


def get_data_statistics(data_path="data/horse2zebra", save=False, load=False):
    images = get_real_images(data_path)
    means, covs = [], []
    for i, image in enumerate(images):
        mean_fname = f"mean-{i}.npy"
        cov_fname = f"cov-{i}.npy"
        if not load:
            mean, cov = get_inception_statistics(image)
        else:
            mean, cov = np.load(mean_fname), np.load(cov_fname)
        means.append(mean)
        covs.append(cov)
        if save and not load:
            np.save(mean_fname, mean)
            np.save(cov_fname, cov)
    return means, covs


if __name__ == '__main__':
    args = parse_cmd()
    data_means, data_covs = get_data_statistics(args.data_path, load=False)
    epochs, (a2b_means, a2b_covs), (b2a_means, b2a_covs) = get_synthetic_statistics(args.log_path)

    a2b_mean, a2b_cov = data_means[0], data_covs[0]
    b2a_mean, b2a_cov = data_means[1], data_covs[1]

    distances_a2b = {key: [] for key in KEYS}
    distances_b2a = {key: [] for key in KEYS}

    # construct distance dictionaries
    for key in KEYS:
        a2b_syn_mean, a2b_syn_cov = a2b_means[key], a2b_covs[key]
        b2a_syn_mean, b2a_syn_cov = b2a_means[key], b2a_covs[key]
        for i, epoch in enumerate(epochs):
            distances_a2b[key].append(np.abs(frechet_distance(a2b_mean, a2b_cov, a2b_syn_mean[i], a2b_syn_cov[i])))
            distances_b2a[key].append(np.abs(frechet_distance(b2a_mean, b2a_cov, b2a_syn_mean[i], b2a_syn_cov[i])))

    save_dict = {'epochs': epochs,
                 'distances_b2a': distances_a2b,  # names are inverted in the main.py
                 'distances_a2b': distances_b2a}

    with open(os.path.join(args.log_path, "fid.json"), "w")as f:
        json.dump(save_dict, f)

    # plot
    with mpl.rc_context(PLOT_CONTEXT):
        fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(10, 4))

        # a2b
        for key in distances_a2b:
            axs[0].plot(epochs, distances_a2b[key])
        axs[0].legend(KEYS)
        axs[0].set_title('Zebra-to-Horse')
        axs[0].set_xlabel('epochs')
        axs[0].set_ylabel('FID')

        # b2a
        for key in distances_b2a:
            axs[1].plot(epochs, distances_b2a[key])
        axs[1].legend(KEYS)
        axs[1].set_title('Horse-to-Zebra')
        axs[1].set_xlabel('epochs')
        axs[1].set_ylabel('FID')

        plt.tight_layout()
        plt.savefig(os.path.join(args.log_path, "fid.pdf"))
