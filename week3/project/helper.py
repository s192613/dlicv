import os
import torch
import json
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
from week3.project.networks import Discriminator, Generator
from week3.project.cycle_dataset import CycleDataset


def prepare_data(data_path, batch_size=1):
    transform = transforms.Compose([transforms.ToTensor()])

    train_dataset = CycleDataset(train=True, transform=transform, data_path=data_path)
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=1)

    test_dataset = CycleDataset(train=False, transform=transform, data_path=data_path)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=1)

    return {"train": (train_dataset,
                      train_loader),
            "test": (test_dataset,
                     test_loader)}


def get_gan_loss_fun(loss_name):
    """return callable (y_pred, y_true)"""
    if loss_name.lower() == 'lsgan':
        return torch.nn.MSELoss()
    if loss_name.lower() == 'bce':
        return torch.nn.BCEWithLogitsLoss()
    elif loss_name.lower() == 'wgan':
        raise NotImplementedError()


def get_img_loss_fun(loss_name):
    """returns callable (y_pred, y_true)."""
    if loss_name.lower() == 'l1':
        return torch.nn.L1Loss()
    elif loss_name.lower() == 'l2':
        return torch.nn.MSELoss()


def get_networks(dropout_rate=.0, resnet_blocks=6):
    return {"gen_ab": Generator(dropout_rate=dropout_rate, blocks=resnet_blocks),
            "gen_ba": Generator(dropout_rate=dropout_rate, blocks=resnet_blocks),
            "discr_a": Discriminator(),
            "discr_b": Discriminator()}


def get_optimizers(networks, optimizer_type, lr_gen, lr_discr):
    g_params = list(networks["gen_ab"].parameters()) + list(networks["gen_ba"].parameters())
    d_params = list(networks["discr_a"].parameters()) + list(networks["discr_b"].parameters())

    if optimizer_type.lower() == 'sgd':
        optim_g = torch.optim.SGD(g_params, lr_gen, momentum=0.5)
        optim_d = torch.optim.SGD(d_params, lr_discr, momentum=0.5)
    elif optimizer_type.lower() == 'adam':
        optim_g = torch.optim.Adam(g_params, lr_gen, betas=(0.5, 0.999))
        optim_d = torch.optim.Adam(d_params, lr_discr, betas=(0.5, 0.999))
    else:
        raise NotImplementedError()

    return {"gen": optim_g, "discr": optim_d}


def get_version(name="cycle-gan", logdir='logs/', width=3):
    os.makedirs(logdir, exist_ok=True)
    files = list(sorted([f for f in os.listdir(logdir) if f"{name}-v" in f]))
    if len(files) < 1:
        version = '1'.rjust(width, '0')
    else:
        last_version = int(files[-1][-width:])
        version = str(last_version + 1).rjust(width, '0')
    return version


def save_config(path, args):
    with open(os.path.join(path, "config.json"), "w") as json_f:
        json.dump(vars(args), json_f)


def read_config(path):
    with open(os.path.join(path, "config.json")) as json_f:
        return json.load(json_f)


def load_checkpoint(log_dir, epoch, device):
    if epoch == -1:
        epoch = max(map(lambda s: int(s[0:-4].split('_')[1]), [f for f in os.listdir(log_dir) if "checkpoint" in f]))
    config = read_config(log_dir)
    fname = f"checkpoint_{epoch}.tar"
    path = os.path.join(log_dir, fname)
    checkpoint = torch.load(path, map_location=device)

    nets = get_networks(dropout_rate=config["dropout_rate"], resnet_blocks=config["resnet_blocks"])
    for name, net in nets.items():
        net.load_state_dict(checkpoint[f"{name}_state_dict"])

    optimizers = get_optimizers(nets, config["optimizer"], config["lr_gen"], config["lr_discr"])
    optimizers["gen"].load_state_dict(checkpoint["gen_opt_state_dict"])
    optimizers["discr"].load_state_dict(checkpoint["discr_pt_state_dict"])

    return nets, optimizers
