import torch.nn as nn
import torch.nn.functional as F


class Discriminator(nn.Module):

    def __init__(self):
        super(Discriminator, self).__init__()

        base_d = 64

        self.conv0 = nn.Sequential(nn.Conv2d(3, base_d, 4, stride=2, padding=1),
                                   nn.LeakyReLU(0.2))  # no batchnorm as per paper

        self.conv1 = nn.Sequential(nn.Conv2d(base_d, base_d * 2, 4, stride=2, padding=1, bias=False),
                                   nn.BatchNorm2d(base_d * 2),
                                   nn.LeakyReLU(0.2)
                                   )
        self.conv2 = nn.Sequential(nn.Conv2d(base_d * 2, base_d * 4, 4, stride=2, padding=1, bias=False),
                                   nn.BatchNorm2d(base_d * 4),
                                   nn.LeakyReLU(0.2)
                                   )
        self.conv3 = nn.Sequential(nn.Conv2d(base_d * 4, base_d * 8, 4, stride=1, padding=1, bias=False),
                                   nn.BatchNorm2d(base_d * 8),
                                   nn.LeakyReLU(0.2)
                                   )

        self.conv4 = nn.Sequential(nn.Conv2d(base_d * 8, 1, 4, stride=1, padding=1)
                                   )

    def forward(self, x):
        x = self.conv0(x)
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)

        return x


norm_layer = nn.InstanceNorm2d


class ResBlock(nn.Module):
    def __init__(self, in_channels, out_channels, dropout_rate=.0):
        super(ResBlock, self).__init__()
        self.conv = nn.Sequential(nn.Conv2d(in_channels, out_channels, 3, 1, 1),
                                  norm_layer(out_channels),
                                  nn.ReLU(),
                                  nn.Dropout2d(p=dropout_rate),
                                  nn.Conv2d(out_channels, out_channels, 3, 1, 1))
        self.norm = norm_layer(out_channels)

    def forward(self, x):
        return F.relu(self.norm(self.conv(x) + x))


class Generator(nn.Module):
    def __init__(self, f=64, blocks=6, dropout_rate=.0):
        super(Generator, self).__init__()

        layers = [  # Initial Layer
            nn.ReflectionPad2d(3),
            nn.Conv2d(3, f, 7, 1, 0),
            norm_layer(f),
            nn.ReLU(),

            # Downsampling 1
            nn.Conv2d(f, 2 * f, 3, 2, 1),
            norm_layer(2 * f),
            nn.ReLU(),

            # Downsampling 2
            nn.Conv2d(2 * f, 4 * f, 3, 2, 1),
            norm_layer(4 * f),
            nn.ReLU()]

        # Should be 64x64x256

        # ResNet blocks
        for i in range(int(blocks)):
            layers.append(ResBlock(in_channels=4 * f, out_channels=4 * f, dropout_rate=dropout_rate))

        layers.extend([

            nn.ConvTranspose2d(4 * f, 4 * 2 * f, 3, 1, 1),
            nn.PixelShuffle(2),
            norm_layer(2 * f),
            nn.ReLU(),

            nn.ConvTranspose2d(2 * f, 4 * f, 3, 1, 1),
            nn.PixelShuffle(2),
            norm_layer(f),
            nn.ReLU(),

            nn.ReflectionPad2d(3),
            nn.Conv2d(f, 3, 7, 1, 0),
            nn.Tanh()])

        self.conv = nn.Sequential(*layers)

    def forward(self, x):
        return self.conv(x)
