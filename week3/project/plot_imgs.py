import pickle
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import glob
import PIL.Image as Image
import numpy as np

PLOT_CONTEXT = {
    'text.usetex': False,
    'font.size': 10,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
    'axes.formatter.useoffset': False,
    # "text.latex.preamble": [r'\usepackage{cmbright}']
}
if __name__ == '__main__':
    log_paths = ["logs/cycle-gan-v006"] #, "logs/cycle-gan-v005", "logs/cycle-gan-v006", "logs/cycle-gan-v007"]
    data_path = "../../data/horse2zebra/test"

    epoch = 29
    directions = ["A2B", "B2A"]
    modes = ["Fake", "Reconstructed", "Identity"]


    image_ids = {"A2B": [40, 21, 43, 60],
                 "B2A": [20, 1, 60, 80]}

    # Hard-coded because ids don't align
    image_ids2 = {"A2B": ["n02381460_7230", "n02381460_7660", "n02381460_4740", "n02381460_1110"],
                 "B2A": ["n02391049_9400", "n02391049_8020", "n02391049_400", "n02391049_5030"]}


    with mpl.rc_context(PLOT_CONTEXT):
        for l, log_path in enumerate(log_paths):
            fig, axs = plt.subplots(4, len(image_ids["A2B"]) + len(image_ids["B2A"]),
                                    figsize=(10, 5), gridspec_kw={'hspace': 0, 'wspace': 0})
            # Plot real images
            for i, id in enumerate(image_ids2["A2B"]):
                axs[0, i].set_yticklabels([])
                axs[0, i].set_xticklabels([])
                axs[0, 0].set_ylabel("Real", rotation=0, size='large')
                axs[0, i].imshow(Image.open(os.path.join(data_path, 'A', f'{id}.jpg')).convert('RGB'))

            # Plot generated images
            for i, id in enumerate(image_ids2["B2A"]):
                axs[0, len(image_ids["A2B"]) + i].set_yticklabels([])
                axs[0, len(image_ids["A2B"]) + i].set_xticklabels([])
                axs[0, len(image_ids["A2B"]) + i].imshow(Image.open(os.path.join(data_path, 'B', f'{id}.jpg')).convert('RGB'))

                for direction in directions:
                    base_id = 0
                    if direction == "B2A":
                        base_id = len(image_ids["A2B"])
                    with open(os.path.join(log_path, f"images{direction}_{epoch}.pckle"), 'rb') as f:
                        images = pickle.load(f)
                        for j, mode in enumerate(modes):
                            for k, id in enumerate(image_ids[direction]):
                                axs[1 + j, base_id + k].set_yticklabels([])
                                axs[1 + j, base_id + k].set_xticklabels([])
                                axs[1 + j, 0].set_ylabel(mode, rotation=0, size='large')
                                image = images[mode][id]
                                image = np.squeeze(image)
                                image = np.swapaxes(image, 0, 2)
                                image = np.swapaxes(image, 0, 1)
                                axs[1 + j, base_id + k].imshow(image)

            plt.savefig(f'{log_path}/model_{l+1}_epoch_{epoch}.pdf')
            fig.tight_layout()
            plt.show()




