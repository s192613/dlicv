import numpy as np
import pickle

from week3.project.fid import get_real_images

path = "logs/cycle-gan-v023"
A2B_fname = f"{path}/imagesA2B_4.pckle"
B2A_fname = f"{path}/imagesB2A_4.pckle"
KEY = "Reconstructed"


def match_images(images_source, images_target):
    """return distance matrix (rows: sources - columns: targets)"""
    distances = np.zeros((len(images_source), len(images_target)))
    for i_s in range(len(images_source)):
        for i_t in range(len(images_source)):
            distances[i_s, i_t] = np.sum((images_source[i_s, ...] - images_target[i_t, ...]) ** 2)
    return distances, np.argmin(distances, axis=1).tolist()


if __name__ == '__main__':
    real_imagesA, real_imagesB = get_real_images(size=256)
    fake_imagesA, fake_imagesB = np.vstack(pickle.load(open(A2B_fname, 'rb'))[KEY]), np.vstack(
        pickle.load(open(B2A_fname, 'rb'))[KEY])

    print("Images A")
    print(match_images(real_imagesA.numpy(), fake_imagesA)[1])

    print("Images B")
    print(match_images(real_imagesB.numpy(), fake_imagesB)[1])
