import os
import torch
import glob
import PIL.Image as Image


class CycleDataset(torch.utils.data.Dataset):
    def __init__(self, train, transform=None, data_path='data/horse2zebra'):
        self.transform = transform

        data_path = os.path.join(data_path, 'train' if train else 'test')  # train test
        self.imageA_paths = glob.glob(os.path.join(data_path, 'A') + '/*.jpg')
        self.imageB_paths = glob.glob(os.path.join(data_path, 'B') + '/*.jpg')
        print(
            f"Dataset in {data_path} contains {len(self.imageA_paths)} A-images, and {len(self.imageB_paths)} B-images.")

    def __len__(self):
        """Returns the total number of samples"""
        return 2 #min(len(self.imageA_paths), len(self.imageB_paths))

    def __getitem__(self, idx):
        'Generates one sample of data'
        imageA_path = self.imageA_paths[idx % len(self.imageA_paths)]
        imageB_path = self.imageB_paths[idx % len(self.imageB_paths)]

        imageA = Image.open(imageA_path).convert('RGB')
        imageB = Image.open(imageB_path).convert('RGB')

        if self.transform:
            imageA = self.transform(imageA)
            imageB = self.transform(imageB)

        return imageA, imageB
