import week2.project.helper as helper
import torch
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

PLOT_CONTEXT = {
    'text.usetex': True,
    'font.size': 10,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
    'axes.formatter.useoffset': False,
    "text.latex.preamble": [r'\usepackage{cmbright}']
}


def neg_pos_ratio(mask_batch):
    n, p = neg_pos(mask_batch)
    return n.double() / p.double()


def neg_pos(mask_batch):
    n = torch.sum(mask_batch == 0)
    p = torch.sum(mask_batch == 1)
    return n, p


def positive_location(mask_batch):
    _, x, y = torch.where(torch.squeeze(mask_batch) == 1)
    return x.tolist(), y.tolist()


if __name__ == '__main__':
    seed = 42
    torch.manual_seed(seed)

    batch_size = 32
    data_path = "data/LIDC_crops/LIDC_DLCV_version"
    data = helper.prepare_data(data_path, batch_size, False)

    dset, loader = data["train"]
    net_pos_ratio_per_batch = []
    n = 0.0
    p = 0.0
    pix_intensity_pos = []
    pix_intensity_neg = []
    x_pos = []
    y_pos = []
    for i, (img, mask) in enumerate(loader):
        positive_location(mask)
        pix_intensity_pos.extend(img[mask == 1].view(-1).tolist())
        pix_intensity_neg.extend(img[mask == 0].view(-1).tolist())

        x, y = positive_location(mask)
        x_pos.extend(x)
        y_pos.extend(y)

        net_pos_ratio_per_batch.append(neg_pos_ratio(mask))
        n_i, p_i = neg_pos(mask)
        n += n_i
        p += p_i

    print(np.mean(net_pos_ratio_per_batch))
    print(n / p)
    with mpl.rc_context(PLOT_CONTEXT):
        plt.figure()
        sns.distplot(net_pos_ratio_per_batch, kde=False)
        plt.xlabel("negative pix/positive pix")
        plt.ylabel("number of batches")
        plt.tight_layout()
        plt.savefig('ratios.pdf')

        plt.figure()
        plt.bar(["negative", "positive"], [n, p])
        plt.ylabel("Number of pixels")
        plt.tight_layout()
        plt.savefig('counts.pdf')

        fig, ax = plt.subplots(ncols=2, sharex=True)
        ax[0].hist(pix_intensity_neg, bins=100, alpha=.75)
        ax[0].set_ylabel("Number of pixels")
        ax[0].set_xlabel("Pixel intensity")
        ax[0].set_title("Negative class")

        ax[1].hist(pix_intensity_pos, bins=100, alpha=.75)
        ax[1].set_ylabel("Number of pixels")
        ax[1].set_xlabel("Pixel intensity")
        ax[1].set_title("Positive class")
        plt.tight_layout()
        plt.savefig('intensity.pdf')

        plt.figure()
        plt.hist2d(x=x_pos, y=y_pos, cmap=plt.cm.jet, bins=128)
        plt.xlabel("$x$")
        plt.ylabel("$y$")
        plt.xlim(0, 127)
        plt.ylim(0, 127)
        plt.colorbar()
        plt.grid()
        plt.tight_layout()
        plt.savefig('pos_locations.pdf')