import argparse
import torch
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from week2.project.metrices import get_probability, get_class_labels
import week2.project.helper as helper
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

PLOT_CONTEXT = {
    'text.usetex': False,
    'font.size': 10,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
    'axes.formatter.useoffset': False,
    # "text.latex.preamble": [r'\usepackage{cmbright}']
}

def parse_cmd():
    parser = argparse.ArgumentParser(description="script to train segmentation model for the LIDC-IDRI task.")
    parser.add_argument('--dropout_rate', type=float, default=.0,
                        help='dropout to be applied in the network (default: 0.0)')
    parser.add_argument('--data_path', type=str, default='../../data/LIDC_crops/LIDC_DLCV_version/',
                        help='location of the dataset (default: data/LIDC_crops/LIDC_DLCV_version/)')

    args = parser.parse_args()

    return args

def infer(data_path, img_id, binary_mask=False):
    data = helper.prepare_data(data_path, 32, False)
    dset, loader = data["test"]

    img, mask = dset[img_id]
    img = torch.unsqueeze(img, 0)
    mask = torch.unsqueeze(mask, 0)
    model.eval()
    img_d = img.to(device)
    output = model(img_d)
    output = get_probability(output)
    if binary_mask:
        output = get_class_labels(output)
    img = np.swapaxes(img.detach().numpy(), 1, 3)
    mask = np.swapaxes(mask.detach().numpy(), 1, 3)
    output = np.swapaxes(output.detach().numpy(), 1, 3)
    return img, mask, output

if __name__ == '__main__':
    args = parse_cmd()
    img_ids = [1288, 1867, 69, 488, 234, 1345, 345, 223, 664]
    models = ["logs/segnet_vanilla-v010/", #segnet
              "logs/unet/",
              "logs/segnet_vanilla-v016/", # segnet + focal loss
              "logs/segnet_vanilla-v011/", # augmentation
              "logs/segnet_vanilla-v013/", # segnet + augmentation + mixup
              "logs/segnet_vanilla-v008/", # segnet + bse loss weights
              "logs/segnet_vanilla-v012/",  # segnet + dropout
              ]

    samples = {"imgs": [], "masks": [], "pred_masks": []}
    for i, model in enumerate(models):
        data = helper.prepare_data(args.data_path, 32, False)
        model, optim = helper.load_checkpoint(model, -1, device)

        imgs = []
        masks = []
        pred_masks = []
        for img_id in img_ids:
            img, mask, output = infer(data, img_id)
            imgs.append(img)
            masks.append(mask)
            pred_masks.append(output)

        if i == 0:
            samples["imgs"] = imgs
            samples["masks"] = masks
        samples["pred_masks"].append(pred_masks)

    with mpl.rc_context(PLOT_CONTEXT):
        fig, axs = plt.subplots(len(models) + 2, len(img_ids), figsize=(10, 10), gridspec_kw={'hspace': 0, 'wspace': 0})
        row_labels = ["img ", "true ", "pred"]

        for i, img in enumerate(samples["imgs"]):
            axs[0,i].set_yticklabels([])
            axs[0,i].set_xticklabels([])
            axs[0, 0].set_ylabel("img", rotation=0, size='large')
            axs[0,i].imshow(np.squeeze(img), 'gray')

        for i, img in enumerate(samples["masks"]):
            axs[1,i].set_yticklabels([])
            axs[1,i].set_xticklabels([])
            axs[1,0].set_ylabel("true", rotation=0, size='large')
            axs[1, i].imshow(np.squeeze(img), 'gray')

        for j, pred_masks in enumerate(samples["pred_masks"]):
            axs[2 + j, 0].set_ylabel(f"pred\n({j+1})", rotation=0, size='large')
            for i, pred_mask in enumerate(pred_masks):
                axs[2+j,i].set_yticklabels([])
                axs[2+j,i].set_xticklabels([])
                axs[2+j,i].imshow(np.squeeze(pred_mask), 'gray')

        plt.savefig('example_infer.pdf')
        # fig.tight_layout()
        # plt.show()






