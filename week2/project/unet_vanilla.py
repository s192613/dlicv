import torch
import torch.nn as nn
import torch.nn.functional as F


class UNetVanilla(nn.Module):
    def __init__(self, dropout_rate=0.0):
        super().__init__()

        # encoder (downsampling)

        # e0
        self.enc_conv00 = nn.Conv2d(1, 64, 3, padding=1)  # same
        self.bn_e00 = nn.BatchNorm2d(64)
        self.enc_conv01 = nn.Conv2d(64, 64, 3, padding=1)  # same (128)
        self.bn_e01 = nn.BatchNorm2d(64)
        self.pool0 = nn.MaxPool2d(2, 2, padding=0)  # 128 -> 64

        # e1
        self.enc_conv10 = nn.Conv2d(64, 128, 3, padding=1)  # same
        self.bn_e10 = nn.BatchNorm2d(128)
        self.enc_conv11 = nn.Conv2d(128, 128, 3, padding=1)  # same #!!! (64)
        self.bn_e11 = nn.BatchNorm2d(128)
        self.pool1 = nn.MaxPool2d(2, 2, padding=0)  # 64 -> 32 #!!!

        # e2
        self.enc_conv20 = nn.Conv2d(128, 256, 3, padding=1)  # same
        self.bn_e20 = nn.BatchNorm2d(256)
        self.enc_conv21 = nn.Conv2d(256, 256, 3, padding=1)  # same (32)
        self.bn_e21 = nn.BatchNorm2d(256)
        self.pool2 = nn.MaxPool2d(2, 2, padding=0)  # 32 -> 16

        # e3
        self.enc_conv30 = nn.Conv2d(256, 512, 3, padding=1)  # same
        self.bn_e30 = nn.BatchNorm2d(512)
        self.enc_conv31 = nn.Conv2d(512, 512, 3, padding=1)  # same (16)
        self.bn_e31 = nn.BatchNorm2d(512)
        self.pool3 = nn.MaxPool2d(2, 2, padding=0)  # 16 -> 8

        # bottleneck
        self.bottleneck_conv0 = nn.Conv2d(512, 1024, 3, padding=1)  # same
        self.bn_b0 = nn.BatchNorm2d(1024)
        self.bottleneck_conv1 = nn.Conv2d(1024, 512, 3, padding=1)  # same
        self.bn_b1 = nn.BatchNorm2d(512)

        # decoder (upsampling)
        # d0
        self.upsample0 = nn.ConvTranspose2d(512, 512, 2, stride=2, padding=0, output_padding=0)  # 8 -> 16
        self.dec_conv00 = nn.Conv2d(512 + 512, 512, 3, padding=1)  # 64+64=128 because of skip  (16)
        self.bn_d00 = nn.BatchNorm2d(512)
        self.dec_conv01 = nn.Conv2d(512, 256, 3, padding=1)
        self.bn_d01 = nn.BatchNorm2d(256)

        # d1
        self.upsample1 = nn.ConvTranspose2d(256, 256, 2, stride=2, padding=0, output_padding=0)  # 16 -> 32 #!!!
        self.dec_conv10 = nn.Conv2d(256 + 256, 256, 3, padding=1)  # 64+64=128 because of skip (32)
        self.bn_d10 = nn.BatchNorm2d(256)
        self.dec_conv11 = nn.Conv2d(256, 128, 3, padding=1)  # !!! # 32....
        self.bn_d11 = nn.BatchNorm2d(128)

        # d2
        self.upsample2 = nn.ConvTranspose2d(128, 128, 2, stride=2, padding=0, output_padding=0)  # 32 -> 64
        self.dec_conv20 = nn.Conv2d(128 + 128, 128, 3, padding=1)  # 64+64=128 because of skip (64)
        self.bn_d20 = nn.BatchNorm2d(128)
        self.dec_conv21 = nn.Conv2d(128, 64, 3, padding=1)  # !!!
        self.bn_d21 = nn.BatchNorm2d(64)

        # d3
        self.upsample3 = nn.ConvTranspose2d(64, 64, 2, stride=2, padding=0, output_padding=0)  # 64 -> 128
        self.dec_conv30 = nn.Conv2d(128, 64, 3, padding=1)  # 64+64=128 because of skip (128)
        self.bn_d30 = nn.BatchNorm2d(64)
        self.dec_conv31 = nn.Conv2d(64, 64, 3, padding=1)
        self.bn_d31 = nn.BatchNorm2d(64)
        self.dec_conv32 = nn.Conv2d(64, 1, 1, padding=0)

    def forward(self, x):
        # encoder
        e00s = F.relu(self.bn_e00(self.enc_conv00(x)))
        e01s = F.relu(self.bn_e01(self.enc_conv01(e00s)))
        e0 = self.pool0(e01s)

        e10s = F.relu(self.bn_e10(self.enc_conv10(e0)))
        e11s = F.relu(self.bn_e11(self.enc_conv11(e10s)))  # !!! 64
        e1 = self.pool1(e11s)

        e20s = F.relu(self.bn_e20(self.enc_conv20(e1)))
        e21s = F.relu(self.bn_e21(self.enc_conv21(e20s)))
        e2 = self.pool2(e21s)

        e30s = F.relu(self.bn_e30(self.enc_conv30(e2)))
        e31s = F.relu(self.bn_e31(self.enc_conv31(e30s)))
        e3 = self.pool3(e31s)

        # bottleneck
        b0 = F.relu(self.bn_b0(self.bottleneck_conv0(e3)))
        b1 = F.relu(self.bn_b1(self.bottleneck_conv1(b0)))

        # decoder
        d00 = F.relu(self.bn_d00(self.dec_conv00(torch.cat([self.upsample0(b1), e31s], 1))))
        d01 = F.relu(self.bn_d01(self.dec_conv01(d00)))

        d10 = F.relu(self.dec_conv10(torch.cat([self.bn_d10(self.upsample1(d01)), e21s], 1)))
        d11 = F.relu(self.bn_d11(self.dec_conv11(d10)))

        d20 = F.relu(self.dec_conv20(torch.cat([self.bn_d20(self.upsample2(d11)), e11s], 1)))  # !!!
        d21 = F.relu(self.bn_d21(self.dec_conv21(d20)))

        d30 = self.dec_conv30(torch.cat([self.bn_d30(self.upsample3(d21)), e01s], 1))  # no activation
        d31 = F.relu(self.bn_d31(self.dec_conv31(d30)))
        d32 = F.relu(self.dec_conv32(d31))

        return d32
