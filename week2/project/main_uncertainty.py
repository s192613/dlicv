import torch
import tqdm
import numpy as np
import os
import argparse
from torch.utils.tensorboard import SummaryWriter

import week2.project.helper as helper

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def parse_cmd():
    parser = argparse.ArgumentParser(description="script to train segmentation model for the LIDC-IDRI task.")
    parser.add_argument('--mode', type=str, choices=('ensemble', 'dropout'), default='dropout',
                        help='uncertainty quantification mode.')
    parser.add_argument('--model', type=str,
                        choices=('segnet', 'segnet_small', 'segnet_vanilla', 'unet', 'unet_vanilla'),
                        default="unet",
                        help='model to be trained (default: segnet)')
    parser.add_argument('--loss', type=str, choices=('bce', 'dice', 'focal'), default='bce',
                        help='loss used to train the model (default: bce)')
    parser.add_argument('--regularization', type=str, choices=('none', 'centered', 'sparsity', 'continuity'),
                        default='none', help='regularization to be applied to train the model (default: none)')
    parser.add_argument('--metrics', type=str, nargs='+',
                        choices=('accuracy', 'sensitivity', 'specificity', 'dice', 'iou', 'all'), default=['all'],
                        help='metrics to be monitored during training (default: all)')
    parser.add_argument('--batch_size', type=int, default=32, help='input batch size for training (default: 32)')
    parser.add_argument('--num_epochs', type=int, default=50, help='number of training epochs (default: 50)')
    parser.add_argument('--optimizer', type=str, default='adam', choices=('sgd', 'adam', 'rmsprop'),
                        help='optimizer to be used (default:adam)')
    parser.add_argument('--lr', type=float, default=1e-4, help='learning rate (default: 1e-4)')
    parser.add_argument('--lr_step', type=int, default=-1, help='learning rate decrease step (default: -1)')
    parser.add_argument('--weight_decay', type=float, default=.0, help='weight decay, L2 penalty (default: 0.0)')
    parser.add_argument('--dropout_rate', type=float, default=.0,
                        help='dropout to be applied in the network (default: 0.0)')
    parser.add_argument('--logdir', type=str, default='logs',
                        help='directory where the logs will be saved (default: logs/)')
    parser.add_argument('--save_every', type=int, default=5,
                        help='frequency at which the network should be saved (default: 5)')
    parser.add_argument('--data_path', type=str, default='data/LIDC_crops/LIDC_DLCV_version/',
                        help='location of the dataset (default: data/LIDC_crops/LIDC_DLCV_version/)')
    parser.add_argument('--augmentation', type=bool, default=False,
                        help='whether standart data augmentation should be applied (default: False)')
    parser.add_argument('--weight_imbalance_ratio', type=float, default=1.0,
                        help='how the positive class should be weighted.')
    parser.add_argument('--mixup', type=bool, default=False,
                        help='whether mixup augmentation should be applied.')

    args = parser.parse_args()

    return args


def train(args, loss_fun, data, writer, log_path, metrics):
    assert args.mode == "ensemble" or (args.dropout_rate > 0.0 and args.mode == "dropout")
    n_annotations = data["train"][0][0][1].shape[0]
    folds = list(data.keys())

    for idx in tqdm.tqdm(range(n_annotations), unit='model'):
        model = helper.get_network(args.model, **{"dropout_rate": args.dropout_rate})
        optimizer = helper.get_optimizer(args.optimizer, model.parameters(), args.lr, args.weight_decay)

        model.to(device)
        for epoch in tqdm.tqdm(range(args.num_epochs), unit='epoch'):
            print(f"\n------------- EPOCH {epoch}/{args.num_epochs} ---------------")

            fold_metrics = {fold: {metric: [] for metric in metrics} for fold in folds}
            fold_losses = {fold: [] for fold in folds}

            for fold in tqdm.tqdm(folds, total=len(folds), unit='fold'):
                dset, loader = data[fold]

                # set eval mode if validation or test
                model.train(fold not in ("val", "test"))

                for minibatch_no, (img, target) in tqdm.tqdm(enumerate(loader), total=len(loader), unit='batch'):
                    target = target[:, idx, :, :].unsqueeze(1)
                    if fold == "train":
                        optimizer.zero_grad()
                        if args.mixup:
                            img, target = helper.mixup(img, target)
                    img, target = img.to(device), target.to(device)

                    # Forward pass your image through the network
                    output = model(img)
                    # Compute the loss
                    loss = loss_fun(output, target)
                    fold_losses[fold].append(loss.cpu().item())

                    # Backward pass through the network if training
                    if fold == "train":
                        loss.backward()
                        optimizer.step()

                    # evaluate all the metrics
                    for metric, metric_fun in metrics.items():
                        fold_metrics[fold][metric].append(metric_fun(output, target).cpu().item())

            # print and log loss
            for fold in folds:
                avg_loss = np.mean(fold_losses[fold])
                writer.add_scalar(f'model-{idx}/loss/{fold}', avg_loss, global_step=epoch)
                print(f"{fold} loss: {avg_loss:.3f}", end='\t')
            print()

            # print and log metrics
            for metric in metrics:
                print()
                for fold in folds:
                    mean_metric = np.mean(fold_metrics[fold][metric])
                    writer.add_scalar(f"model-{idx}/{metric}/{fold}", mean_metric, global_step=epoch)
                    print(f"{fold} {metric}: {mean_metric:.3f}", end='\t')
            print()

            # Save
            if (epoch + 1) % args.save_every == 0:
                torch.save({"net_state_dict": model.state_dict(),
                            "opt_state_dict": optimizer.state_dict()},
                           os.path.join(log_path, f"model_{idx}_checkpoint_{epoch}.tar"))


if __name__ == '__main__':
    args = parse_cmd()

    # Parse args
    data = helper.prepare_data(args.data_path, args.batch_size, args.augmentation,
                               mode="first" if args.mode == "dropout" else "all")
    metrics = helper.get_metrics(args.metrics)
    loss_fun = helper.get_loss_fun(args.loss, args.regularization, weight_imbalance_ratio=args.weight_imbalance_ratio,
                                   device=device)

    # Logging
    base_name = f"{args.model}-{args.mode}"
    net_version = helper.get_version(name=base_name, logdir=args.logdir)
    log_path = os.path.join(args.logdir, base_name + f"-v{net_version}")
    writer = SummaryWriter(log_path)
    helper.save_config(log_path, args)

    # Train
    train(args, loss_fun, data, writer, log_path, metrics)
