import torch
import torch.nn as nn
import torch.nn.functional as F

class UNet(nn.Module):
    def __init__(self, dropout_rate=0.0):
        super().__init__()
        self.dropout_rate = dropout_rate

        # encoder (downsampling)
        self.enc_conv0 = nn.Conv2d(1, 64, 3, padding=1) # same
        self.bn_e0 = nn.BatchNorm2d(64)
        self.pool0 = nn.MaxPool2d(2, 2, padding=0)

        self.enc_conv1 = nn.Conv2d(64, 128, 3, padding=1) # same
        self.bn_e1 = nn.BatchNorm2d(128)
        self.pool1 = nn.MaxPool2d(2, 2, padding=0)


        self.enc_conv2 = nn.Conv2d(128, 256, 3, padding=1) # same
        self.doe_2 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e2 = nn.BatchNorm2d(256)
        self.pool2 = nn.MaxPool2d(2, 2, padding=0)

        
        self.enc_conv3 = nn.Conv2d(256, 512, 3, padding=1) # same
        self.doe_3 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e3 = nn.BatchNorm2d(512)
        self.pool3 = nn.MaxPool2d(2, 2, padding=0)


        # bottleneck
        self.bottleneck_conv = nn.Conv2d(512, 1024, 3, padding=1) # same
        self.bn_b = nn.BatchNorm2d(1024)

        # decoder (upsampling)
        self.upsample0 = nn.ConvTranspose2d(1024, 512, 2, stride= 2, padding= 0, output_padding= 0) # 16 -> 32
        self.dec_conv0 = nn.Conv2d(1024, 512, 3, padding=1) # 64+64=128 because of skip
        self.dod_0 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d0 = nn.BatchNorm2d(512)
        
        self.upsample1 = nn.ConvTranspose2d(512, 256, 2, stride = 2, padding= 0, output_padding= 0) # 32 -> 64
        self.dec_conv1 = nn.Conv2d(512, 256, 3, padding=1) # 64+64=128 because of skip
        self.dod_1 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d1 = nn.BatchNorm2d(256)
        
        self.upsample2 = nn.ConvTranspose2d(256, 128, 2, stride= 2, padding= 0, output_padding= 0) # 64 -> 128
        self.dec_conv2 = nn.Conv2d(256, 128, 3, padding=1) # 64+64=128 because of skip
        self.bn_d2 = nn.BatchNorm2d(128)
        
        self.upsample3 = nn.ConvTranspose2d(128, 64, 2, stride= 2, padding= 0, output_padding= 0) # 128 -> 256
        self.dec_conv3 = nn.Conv2d(128, 64, 3, padding=1) # 64+64=128 because of skip
        self.bn_d3 = nn.BatchNorm2d(64)

        self.dec_conv4 = nn.Conv2d(64, 1, 3, padding=1) 

    def forward(self, x):
        # encoder
        e0s = F.relu(self.bn_e0(self.enc_conv0(x)))
        e0 = self.pool0(e0s)
        
        e1s = F.relu(self.bn_e1(self.enc_conv1(e0)))
        e1 = self.pool1(e1s)
        
        e2s = F.relu(self.bn_e2(self.doe_2(self.enc_conv2(e1))))
        e2 = self.pool2(e2s)
        
        e3s = F.relu(self.bn_e3(self.doe_3(self.enc_conv3(e2))))
        e3 = self.pool3(e3s)

        # bottleneck
        b = F.relu(self.bn_b(self.bottleneck_conv(e3)))

        # decoder
        d0 = F.relu(self.dec_conv0(torch.cat([self.bn_d0(self.dod_0(self.upsample0(b))),e3s],1)))

        d1 = F.relu(self.dec_conv1(torch.cat([self.bn_d1(self.dod_1(self.upsample1(d0))),e2s],1)))

        d2 = F.relu(self.dec_conv2(torch.cat([self.bn_d2(self.upsample2(d1)),e1s],1)))

        d3 = self.dec_conv3(torch.cat([self.bn_d3(self.upsample3(d2)), e0s],1))  # no activation

        d4 = self.dec_conv4(d3)
        
        return d4
