import os
import json
from torch.utils.data import DataLoader
import torch
import torch.nn as nn

from week2.project.dataloaders.lidc import LIDC
from week2.project.metrices import metrics_dict, get_probability, get_class_labels
from week2.project.regularizations import regularizations_dict
from week2.project.losses import dice_loss, focal_loss
from week2.project.unet_small import UNet
from week2.project.unet_vanilla import UNetVanilla
from week2.project.segnet import SegNet
from week2.project.segnet_vanilla import SegNetVanilla


def prepare_data(data_path, batch_size=64, augmentation=None, mode="first"):
    train_set = LIDC("train", root_path=data_path, mode=mode, augmentation=augmentation)
    val_set = LIDC("val", root_path=data_path, mode=mode, augmentation=augmentation)
    test_set = LIDC("test", root_path=data_path, mode=mode, augmentation=augmentation)

    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=1)
    val_loader = DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=1)
    test_loader = DataLoader(test_set, batch_size=batch_size, shuffle=False, num_workers=1)

    data = {"train": (train_set, train_loader),
            "val": (val_set, val_loader),
            "test": (test_set, test_loader)}
    return data


def get_optimizer(name, net_parameters, lr, weight_decay):
    if name.lower() == 'sgd':
        return torch.optim.SGD(net_parameters, lr, weight_decay=weight_decay, momentum=0.9)
    elif name.lower() == 'adam':
        return torch.optim.Adam(net_parameters, lr, weight_decay=weight_decay)
    elif name.lower() == "rmsprop":
        return torch.optim.RMSprop(net_parameters, lr, weight_decay=weight_decay)
    else:
        raise NotImplementedError()


def get_loss_fun(loss_name, reg_name, weight_imbalance_ratio=1.0, device='cpu'):
    # XXX: network should output logit
    if loss_name.lower() == 'bce':
        weight = torch.FloatTensor([weight_imbalance_ratio]).to(device)
        loss_fun = nn.BCEWithLogitsLoss(pos_weight=weight)
    elif loss_name.lower() == 'dice':
        loss_fun = dice_loss
    elif loss_name.lower() == 'focal':
        loss_fun = focal_loss
    else:
        NotImplementedError()

    def reg_loss(output, target):
        loss_ = loss_fun(output, target)
        reg_ = regularizations_dict[reg_name](output, target, reduction=loss_fun.reduction)
        return loss_ + reg_

    return reg_loss if reg_name != 'none' else loss_fun


def get_metrics(names):
    if 'all' in names:
        names = list(metrics_dict.keys())
    return {name: metrics_dict[name] for name in names}


def get_network(name, **kwargs):
    if name.lower() == "segnet":
        return SegNet(**kwargs)
    elif name.lower() == "segnet_vanilla":
        return SegNetVanilla(**kwargs)
    elif name.lower() == "unet":
        return UNet(**kwargs)
    elif name.lower() == "unet_vanilla":
        return UNetVanilla(**kwargs)
    else:
        NotImplementedError()


def get_version(name="dense-net", logdir='logs/', width=3):
    os.makedirs(logdir, exist_ok=True)
    files = list(sorted([f for f in os.listdir(logdir) if f"{name}-v" in f]))
    if len(files) < 1:
        version = '1'.rjust(width, '0')
    else:
        last_version = int(files[-1][-width:])
        version = str(last_version + 1).rjust(width, '0')
    return version


def save_config(path, args):
    with open(os.path.join(path, "config.json"), "w") as json_f:
        json.dump(vars(args), json_f)


def read_config(path):
    with open(os.path.join(path, "config.json")) as json_f:
        return json.load(json_f)


def load_checkpoint(log_dir, epoch, device):
    if epoch == -1:
        epoch = max(map(lambda s: int(s[0:-4].split('_')[1]), [f for f in os.listdir(log_dir) if "checkpoint" in f]))
    config = read_config(log_dir)
    fname = f"checkpoint_{epoch}.tar"
    path = os.path.join(log_dir, fname)
    checkpoint = torch.load(path, map_location=device)

    net = get_network(config["model"], dropout_rate=config["dropout_rate"])
    net.load_state_dict(checkpoint["net_state_dict"])
    opt = get_optimizer(config["optimizer"], net.parameters(), config["lr"], config["weight_decay"])
    opt.load_state_dict(checkpoint["opt_state_dict"])

    return net, opt


def out_to_mask(y_pred):
    return get_class_labels(get_probability(y_pred))


def mixup(img, target, alpha=1):
    n = img.shape[0]
    perm = torch.randperm(n)
    perm_img, perm_target = img[perm], target[perm]
    lambd = torch.distributions.beta.Beta(alpha, alpha).sample()  # draw for Beta(alpha, alpha)

    mix_img = lambd * img + (1 - lambd) * perm_img
    mix_target = lambd * target + (1 - lambd) * perm_target

    return mix_img, mix_target


if __name__ == '__main__':
    i = torch.randn((10, 24, 24))
    t = torch.randint(0, 2, (10, 24, 24))
    mix_i, mix_t = mixup(i, t)
    print(mix_i, mix_t)
