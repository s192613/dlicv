import torch
import tqdm
import numpy as np
import os
import argparse
from torch.utils.tensorboard import SummaryWriter

import week2.project.helper as helper

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def parse_cmd():
    parser = argparse.ArgumentParser(description="script to train segmentation model for the LIDC-IDRI task.")
    parser.add_argument('--model', type=str,
                        choices=('segnet', 'segnet_small', 'segnet_vanilla', 'unet', 'unet_vanilla'),
                        default="unet",
                        help='model to be trained (default: segnet)')
    parser.add_argument('--loss', type=str, choices=('bce', 'dice', 'focal'), default='bce',
                        help='loss used to train the model (default: bce)')
    parser.add_argument('--regularization', type=str, choices=('none', 'centered', 'sparsity', 'continuity'),
                        default='none', help='regularization to be applied to train the model (default: none)')
    parser.add_argument('--metrics', type=str, nargs='+',
                        choices=('accuracy', 'sensitivity', 'specificity', 'dice', 'iou', 'all'), default=['all'],
                        help='metrics to be monitored during training (default: all)')
    parser.add_argument('--batch_size', type=int, default=32, help='input batch size for training (default: 32)')
    parser.add_argument('--num_epochs', type=int, default=50, help='number of training epochs (default: 50)')
    parser.add_argument('--optimizer', type=str, default='adam', choices=('sgd', 'adam', 'rmsprop'),
                        help='optimizer to be used (default:adam)')
    parser.add_argument('--lr', type=float, default=1e-4, help='learning rate (default: 1e-4)')
    parser.add_argument('--lr_step', type=int, default=-1, help='learning rate decrease step (default: -1)')
    parser.add_argument('--weight_decay', type=float, default=.0, help='weight decay, L2 penalty (default: 0.0)')
    parser.add_argument('--dropout_rate', type=float, default=.0,
                        help='dropout to be applied in the network (default: 0.0)')
    parser.add_argument('--logdir', type=str, default='logs',
                        help='directory where the logs will be saved (default: logs/)')
    parser.add_argument('--save_every', type=int, default=5,
                        help='frequency at which the network should be saved (default: 5)')
    parser.add_argument('--data_path', type=str, default='data/LIDC_crops/LIDC_DLCV_version/',
                        help='location of the dataset (default: data/LIDC_crops/LIDC_DLCV_version/)')
    parser.add_argument('--augmentation', type=bool, default=False,
                        help='whether standart data augmentation should be applied (default: False)')
    parser.add_argument('--weight_imbalance_ratio', type=float, default=1.0,
                        help='how the positive class should be weighted.')
    parser.add_argument('--mixup', type=bool, default=False,
                        help='whether mixup augmentation should be applied.')

    args = parser.parse_args()

    return args


def train(model, loss_fun, data, optimizer, scheduler, writer, log_path, num_epochs, save_every, metrics, mixup):
    folds = list(data.keys())
    model.to(device)
    for epoch in tqdm.tqdm(range(num_epochs), unit='epoch'):
        print(f"\n------------- EPOCH {epoch}/{num_epochs} ---------------")

        fold_metrics = {fold: {metric: [] for metric in metrics} for fold in folds}
        fold_losses = {fold: [] for fold in folds}

        for fold in tqdm.tqdm(folds, total=len(folds), unit='fold'):
            dset, loader = data[fold]

            # set eval mode if validation or test
            model.train(fold not in ("val", "test"))

            for minibatch_no, (img, target) in tqdm.tqdm(enumerate(loader), total=len(loader), unit='batch'):
                if fold == "train":
                    optimizer.zero_grad()
                    if mixup:
                        img, target = helper.mixup(img, target)
                img, target = img.to(device), target.to(device)

                # Forward pass your image through the network
                output = model(img)
                # Compute the loss
                loss = loss_fun(output, target)
                fold_losses[fold].append(loss.cpu().item())

                # Backward pass through the network if training
                if fold == "train":
                    loss.backward()
                    optimizer.step()

                # evaluate all the metrics
                for metric, metric_fun in metrics.items():
                    fold_metrics[fold][metric].append(metric_fun(output, target).cpu().item())

        # print and log loss
        for fold in folds:
            avg_loss = np.mean(fold_losses[fold])
            writer.add_scalar(f'loss/{fold}', avg_loss, global_step=epoch)
            print(f"{fold} loss: {avg_loss:.3f}", end='\t')
        print()

        # print and log metrics
        for metric in metrics:
            print()
            for fold in folds:
                mean_metric = np.mean(fold_metrics[fold][metric])
                writer.add_scalar(f"{metric}/{fold}", mean_metric, global_step=epoch)
                print(f"{fold} {metric}: {mean_metric:.3f}", end='\t')
        print()

        # Save
        if (epoch + 1) % save_every == 0:
            torch.save({"net_state_dict": model.state_dict(),
                        "opt_state_dict": optimizer.state_dict()},
                       os.path.join(log_path, f"checkpoint_{epoch}.tar"))
        # scheduler step
        scheduler.step()


if __name__ == '__main__':
    args = parse_cmd()

    # Parse args
    data = helper.prepare_data(args.data_path, args.batch_size, args.augmentation)
    metrics = helper.get_metrics(args.metrics)
    loss_fun = helper.get_loss_fun(args.loss, args.regularization, weight_imbalance_ratio=args.weight_imbalance_ratio,
                                   device=device)
    net = helper.get_network(args.model, **{"dropout_rate": args.dropout_rate})
    opt = helper.get_optimizer(args.optimizer, net.parameters(), args.lr, args.weight_decay)
    scheduler = torch.optim.lr_scheduler.StepLR(opt, step_size=args.num_epochs if args.lr_step < 0 else args.lr_step,
                                                gamma=.1)

    # Logging
    net_version = helper.get_version(name=args.model, logdir=args.logdir)
    log_path = os.path.join(args.logdir, args.model + f"-v{net_version}")
    writer = SummaryWriter(log_path)
    helper.save_config(log_path, args)

    # Train
    train(net, loss_fun, data, opt, scheduler, writer, log_path, args.num_epochs, args.save_every, metrics, args.mixup)
