import torch
import torch.nn.functional as F


def dice_loss(output, target):
    out_prob = torch.sigmoid(output)

    inter = torch.mean(2 * out_prob * target + 1)
    union = torch.mean(out_prob + out_prob + 1)

    return 1 - inter / union


def focal_loss(output, target, gamma=2):
    BCE_loss = F.binary_cross_entropy_with_logits(output, target, reduce=False)
    pt = torch.exp(-BCE_loss)
    F_loss = (1 - pt) ** gamma * BCE_loss
    return torch.mean(F_loss)
