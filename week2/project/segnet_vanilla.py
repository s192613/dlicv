import torch.nn as nn
import torch.nn.functional as F


class SegNetVanilla(nn.Module):
    def __init__(self, dropout_rate=0.0):
        super().__init__()
        self.dropout_rate = dropout_rate

        # ENCODER (downsampling)

        # E Block 0
        self.enc_conv00 = nn.Conv2d(1, 64, 3, padding=1)
        self.bn_e00 = nn.BatchNorm2d(64)
        self.enc_conv01 = nn.Conv2d(64, 64, 3, padding=1)
        self.bn_e01 = nn.BatchNorm2d(64)

        # E Block 1
        self.enc_conv10 = nn.Conv2d(64, 128, 3, padding=1)
        self.bn_e10 = nn.BatchNorm2d(128)
        self.enc_conv11 = nn.Conv2d(128, 128, 3, padding=1)
        self.bn_e11 = nn.BatchNorm2d(128)

        # E Block 2
        self.enc_conv20 = nn.Conv2d(128, 256, 3, padding=1)
        self.bn_e20 = nn.BatchNorm2d(256)
        self.enc_conv21 = nn.Conv2d(256, 256, 3, padding=1)
        self.bn_e21 = nn.BatchNorm2d(256)
        self.enc_conv22 = nn.Conv2d(256, 256, 3, padding=1)
        self.bn_e22 = nn.BatchNorm2d(256)

        # E Block 3
        self.enc_conv30 = nn.Conv2d(256, 512, 3, padding=1)
        self.doe_30 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e30 = nn.BatchNorm2d(512)
        self.enc_conv31 = nn.Conv2d(512, 512, 3, padding=1)
        self.doe_31 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e31 = nn.BatchNorm2d(512)
        self.enc_conv32 = nn.Conv2d(512, 512, 3, padding=1)
        self.doe_32 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e32 = nn.BatchNorm2d(512)

        # E Block 4
        self.enc_conv40 = nn.Conv2d(512, 512, 3, padding=1)
        self.doe_40 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e40 = nn.BatchNorm2d(512)
        self.enc_conv41 = nn.Conv2d(512, 512, 3, padding=1)
        self.doe_41 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e41 = nn.BatchNorm2d(512)
        self.enc_conv42 = nn.Conv2d(512, 512, 3, padding=1)
        self.doe_42 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_e42 = nn.BatchNorm2d(512)

        # DECODER (upsampling)

        # D Block 0
        self.dec_conv00 = nn.Conv2d(512, 512, 3, padding=1)
        self.dod_00 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d00 = nn.BatchNorm2d(512)
        self.dec_conv01 = nn.Conv2d(512, 512, 3, padding=1)
        self.dod_01 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d01 = nn.BatchNorm2d(512)
        self.dec_conv02 = nn.Conv2d(512, 512, 3, padding=1)
        self.dod_02 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d02 = nn.BatchNorm2d(512)

        # D Block 1
        self.dec_conv10 = nn.Conv2d(512, 512, 3, padding=1)
        self.dod_10 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d10 = nn.BatchNorm2d(512)
        self.dec_conv11 = nn.Conv2d(512, 512, 3, padding=1)
        self.dod_11 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d11 = nn.BatchNorm2d(512)
        self.dec_conv12 = nn.Conv2d(512, 256, 3, padding=1)
        self.dod_12 = nn.Dropout(p=self.dropout_rate, inplace=False)
        self.bn_d12 = nn.BatchNorm2d(256)

        # D Block 2
        self.dec_conv20 = nn.Conv2d(256, 256, 3, padding=1)
        self.bn_d20 = nn.BatchNorm2d(256)
        self.dec_conv21 = nn.Conv2d(256, 256, 3, padding=1)
        self.bn_d21 = nn.BatchNorm2d(256)
        self.dec_conv22 = nn.Conv2d(256, 128, 3, padding=1)
        self.bn_d22 = nn.BatchNorm2d(128)

        # D Block 3
        self.dec_conv30 = nn.Conv2d(128, 128, 3, padding=1)
        self.bn_d30 = nn.BatchNorm2d(128)
        self.dec_conv31 = nn.Conv2d(128, 64, 3, padding=1)
        self.bn_d31 = nn.BatchNorm2d(64)

        # D Block 4
        self.dec_conv40 = nn.Conv2d(64, 64, 3, padding=1)
        self.bn_d40 = nn.BatchNorm2d(64)
        self.dec_conv41 = nn.Conv2d(64, 64, 3, padding=1)
        self.bn_d41 = nn.BatchNorm2d(64)

        self.dec_conv42 = nn.Conv2d(64, 1, 3, padding=1)

    def forward(self, x):
        # ENCODER

        # E Block 0
        e00 = F.relu(self.bn_e00(self.enc_conv00(x)))
        e01 = F.relu(self.bn_e01(self.enc_conv01(e00)))

        p_e0, id_e0 = F.max_pool2d(e01, kernel_size=2, stride=2, return_indices=True)  # downsampling 0 128 -> 64

        # E Block 1
        e10 = F.relu(self.bn_e10(self.enc_conv10(p_e0)))
        e11 = F.relu(self.bn_e11(self.enc_conv11(e10)))

        p_e1, id_e1 = F.max_pool2d(e11, kernel_size=2, stride=2, return_indices=True)  # downsampling 1 64 -> 32

        # E Block 2
        e20 = F.relu(self.bn_e20(self.enc_conv20(p_e1)))
        e21 = F.relu(self.bn_e21(self.enc_conv21(e20)))
        e22 = F.relu(self.bn_e22(self.enc_conv22(e21)))

        p_e2, id_e2 = F.max_pool2d(e22, kernel_size=2, stride=2, return_indices=True)  # downsampling 2 32 -> 16

        # E Block 3
        e30 = F.relu(self.bn_e30(self.doe_30(self.enc_conv30(p_e2))))
        e31 = F.relu(self.bn_e31(self.doe_31(self.enc_conv31(e30))))
        e32 = F.relu(self.bn_e32(self.doe_32(self.enc_conv32(e31))))

        p_e3, id_e3 = F.max_pool2d(e32, kernel_size=2, stride=2, return_indices=True)  # downsampling 3  16-> 8

        # E Block 4
        e40 = F.relu(self.bn_e40(self.doe_40(self.enc_conv40(p_e3))))
        e41 = F.relu(self.bn_e41(self.doe_41(self.enc_conv41(e40))))
        e42 = F.relu(self.bn_e42(self.doe_42(self.enc_conv42(e41))))

        p_e4, id_e4 = F.max_pool2d(e42, kernel_size=2, stride=2, return_indices=True)  # downsampling 3  8-> 4

        # DECODER

        # D Block 0
        u_d0 = F.max_unpool2d(p_e4, id_e4, kernel_size=2, stride=2)  # upsample0 4 -> 8

        d00 = F.relu(self.bn_d00(self.dod_00(self.dec_conv00(u_d0))))
        d01 = F.relu(self.bn_d01(self.dod_01(self.dec_conv01(d00))))
        d02 = F.relu(self.bn_d02(self.dod_02(self.dec_conv02(d01))))

        # D Block 1
        u_d1 = F.max_unpool2d(d02, id_e3, kernel_size=2, stride=2)  # upsample1 8 -> 16

        d10 = F.relu(self.bn_d10(self.dod_10(self.dec_conv10(u_d1))))
        d11 = F.relu(self.bn_d11(self.dod_11(self.dec_conv11(d10))))
        d12 = F.relu(self.bn_d12(self.dod_12(self.dec_conv12(d11))))

        # D Block 2
        u_d2 = F.max_unpool2d(d12, id_e2, kernel_size=2, stride=2)  # upsample2 16 -> 32

        d20 = F.relu(self.bn_d20(self.dec_conv20(u_d2)))
        d21 = F.relu(self.bn_d21(self.dec_conv21(d20)))
        d22 = F.relu(self.bn_d22(self.dec_conv22(d21)))

        # D Block 3
        u_d3 = F.max_unpool2d(d22, id_e1, kernel_size=2, stride=2)  # upsample3 32 -> 64

        d30 = F.relu(self.bn_d30(self.dec_conv30(u_d3)))
        d31 = F.relu(self.bn_d31(self.dec_conv31(d30)))

        # D Block 4
        u_d4 = F.max_unpool2d(d31, id_e0, kernel_size=2, stride=2)  # upsample2 64 -> 128

        d40 = F.relu(self.bn_d40(self.dec_conv40(u_d4)))
        d41 = F.relu(self.bn_d41(self.dec_conv41(d40)))

        d42 = self.dec_conv42(d41)  # output, no activation

        return d42
