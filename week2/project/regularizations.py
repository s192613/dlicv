import torch


def contiguity(output, target, reduction='mean'):
    out_prob = torch.sigmoid(output)

    if reduction == 'mean':
        i = torch.mean(torch.abs(out_prob[:, :, 1:, :] - out_prob[:, :, :-1, :]))  # along height
        j = torch.mean(torch.abs(out_prob[:, :, :, 1:] - out_prob[:, :, :, :-1]))  # along width
    else:  # sum
        i = torch.sum(torch.abs(out_prob[:, :, 1:, :] - out_prob[:, :, :-1, :]))  # along height
        j = torch.sum(torch.abs(out_prob[:, :, :, 1:] - out_prob[:, :, :, :-1]))  # along width

    return i + j


def sparsity(output, target, reduction='mean'):
    if reduction == 'mean':
        return torch.mean(torch.sigmoid(output))
    else:
        return torch.sum(torch.sigmoid(output))


def centered(output, target, reduction='mean'):
    half_h, half_w = output.shape[3] // 2, output.shape[3] // 2
    if reduction == 'mean':
        return torch.mean(1 - torch.sigmoid(output[:, :, half_h, half_w]))
    else:
        return torch.sum(1 - torch.sigmoid(output[:, :, half_h, half_w]))


regularizations_dict = {"contiguity": contiguity,
                        "sparsity": sparsity,
                        "centered": centered}
