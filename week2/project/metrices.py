import torch

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def get_probability(y):
    ret = y
    max = torch.max(y)
    min = torch.min(y)
    if max > 1 or min < 0:
        ret = torch.sigmoid(y)
    return ret


def get_class_labels(y):
    return torch.where(y >= 0.5, torch.tensor([1.0]).to(device), torch.tensor([0.0]).to(device))


def accuracy(y_pred, y_real):
    y_pred = get_probability(y_pred)
    y_class = get_class_labels(y_pred)
    # correct labels
    num_batches = y_pred.shape[0]
    correct_pixs = (torch.sum((y_real - y_class) == 0)).double()
    all_pixs = y_real.shape[0] * y_real.shape[2] * y_real.shape[3]
    acc = correct_pixs / all_pixs
    return acc


def sensivity(y_pred, y_real):
    y_pred = get_probability(y_pred)
    y_class = get_class_labels(y_pred)
    sens = 0
    num_batches = y_pred.shape[0]
    for b in range(num_batches):
        p = (torch.add(torch.sum(y_real[b, :, :, :] == 1), 1)).double()
        tp = (torch.add(torch.sum(torch.logical_and(y_class[b, :, :, :] == 1, y_real[b, :, :, :] == 1)), 1)).double()
        sens_temp = tp / p
        sens += sens_temp / num_batches
    return sens


def specifity(y_pred, y_real):
    y_pred = get_probability(y_pred)
    y_class = get_class_labels(y_pred)

    spec = 0
    num_batches = y_pred.shape[0]
    for b in range(num_batches):
        n = (torch.add(torch.sum(y_real[b, :, :, :] == 0), 1)).double()
        tn = (torch.add(torch.sum(torch.logical_and(y_class[b, :, :, :] == 0, y_real[b, :, :, :] == 0)), 1)).double()
        spec_temp = tn / n
        spec += spec_temp / num_batches
    return spec


def dice(y_pred, y_real):
    y_pred = get_probability(y_pred)
    y_class = get_class_labels(y_pred)
    intersection = torch.logical_and(y_class, y_real)
    num_batches = y_pred.shape[0]
    dice_score = 0
    for b in range(num_batches):
        dice_score_temp = (2 * torch.add(torch.sum(intersection[b, :, :, :]), 1)).double() / (
                    torch.add(torch.sum(y_real[b, :, :, :]), 1).double() + torch.add(torch.sum(y_class[b, :, :, :]),
                                                                                     1).double())
        dice_score += dice_score_temp / num_batches
    return dice_score


def iou(y_pred, y_real):
    y_pred = get_probability(y_pred)
    y_class = get_class_labels(y_pred)
    intersection = torch.logical_and(y_class, y_real)
    union = torch.logical_or(y_class, y_real)
    num_batches = y_pred.shape[0]
    iou_score = 0
    for b in range(num_batches):
        iou_score_temp = torch.add(torch.sum(intersection[b, :, :, :]), 1).double() / torch.add(
            torch.sum(union[b, :, :, :]), 1).double()
        iou_score += iou_score_temp / num_batches
    return iou_score


metrics_dict = {"accuracy": accuracy,
                "sensitivity": sensivity,
                "specificity": specifity,
                "dice": dice,
                "iou": iou}

if __name__ == '__main__':
    # Testing

    seed = 1
    torch.manual_seed(seed)
    # batch_size = 100
    # pred = torch.rand((batch_size, 1, 256, 256))
    # true = get_class_labels(torch.rand((batch_size, 1, 256, 256)))
    pred1 = torch.unsqueeze(torch.unsqueeze(torch.tensor([[0.34, 0.21], [0.77, 0.66]]), dim=0), dim=0)
    pred2 = torch.unsqueeze(torch.unsqueeze(torch.tensor([[0.34, 0.21], [0.11, 0.66]]), dim=0), dim=0)
    pred = torch.cat([pred1, pred2], 0)
    true1 = torch.unsqueeze(torch.unsqueeze(torch.tensor([[0, 1], [1, 1]]), dim=0), dim=0)
    true2 = torch.unsqueeze(torch.unsqueeze(torch.tensor([[1, 1], [1, 1]]), dim=0), dim=0)
    true = torch.cat([true1, true2], 0)
    acc = iou(pred, true)
    print(acc)
