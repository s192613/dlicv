import week2.project.helper as helper
from week2.project.make_plots import PLOT_CONTEXT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
import torch

# load the check point, enable dropout, predicts for $N$ samples

if __name__ == '__main__':
    data = helper.prepare_data("data/LIDC_crops/LIDC_DLCV_version/", 5, None, mode="all")

    # dropout net
    drop_net, _ = helper.load_checkpoint("uncertainty/dropout", -1, "cpu")
    n_samples = 10

    # ensemble
    ensemble_nets = [helper.load_checkpoint(f"uncertainty/ensemble/model_{i}", -1, "cpu")[0] for i in range(4)]

    dset, _ = data["test"]
    indices = [0, 11, 21, 31, 41]

    fig, axs = plt.subplots(nrows=7, ncols=5, squeeze=False, figsize=(15, 21))

    with mpl.rc_context(PLOT_CONTEXT):
        # plot images
        for i, ax in enumerate(axs[0]):
            ax.imshow(np.squeeze(dset[indices[i]][0]))
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            if i == 0:
                ax.annotate('CT scan', (-.1, 0.5), xycoords='axes fraction',
                            va='center', rotation=90)

        # plot ground-truth mean and standard dev
        for i, (idx, ax_mean, ax_std) in enumerate(zip(indices, axs[1], axs[2])):
            label = torch.squeeze(dset[idx][1])

            mean = torch.mean(label, dim=0)
            ax_mean.imshow(mean.detach().cpu(), cmap=plt.get_cmap('jet'))
            ax_mean.get_xaxis().set_visible(False)
            ax_mean.get_yaxis().set_visible(False)

            std = torch.std(label, dim=0)
            ax_std.imshow(std.detach().cpu(), cmap=plt.get_cmap('jet'))
            ax_std.get_xaxis().set_visible(False)
            ax_std.get_yaxis().set_visible(False)

            if i == 0:
                ax_mean.annotate('Annotations Mean', (-.1, 0.5), xycoords='axes fraction',
                                 va='center', rotation=90)
                ax_std.annotate('Annotations Std. Dev.', (-.1, 0.5), xycoords='axes fraction',
                                va='center', rotation=90)

        # ENSEMBLE
        for net in ensemble_nets:
            net.eval()
        for i, (ax_mean, ax_std, idx) in enumerate(zip(axs[3], axs[4], indices)):
            ex = dset[idx][0].unsqueeze(0)
            out = torch.stack([torch.sigmoid(net(ex)).squeeze() for net in ensemble_nets], dim=0)

            mean = torch.mean(out, dim=0)
            ax_mean.imshow(mean.detach().numpy(), cmap=plt.get_cmap('jet'))
            ax_mean.get_xaxis().set_visible(False)
            ax_mean.get_yaxis().set_visible(False)

            std = torch.std(out, dim=0)
            ax_std.imshow(std.detach().numpy(), cmap=plt.get_cmap('jet'))
            ax_std.get_xaxis().set_visible(False)
            ax_std.get_yaxis().set_visible(False)

            if i == 0:
                ax_mean.annotate('Ensemble Mean', (-.1, 0.5), xycoords='axes fraction',
                                 va='center', rotation=90)
                ax_std.annotate('Ensemble Std. Dev.', (-.1, 0.5), xycoords='axes fraction',
                                va='center', rotation=90)

            # DROP OUT
        drop_net.train()
        for i, (ax_mean, ax_std, idx) in enumerate(zip(axs[5], axs[6], indices)):
            ex = dset[indices[i]][0].unsqueeze(0)
            out = torch.stack([torch.sigmoid(drop_net(ex)).squeeze() for _ in range(n_samples)], dim=0)

            mean = torch.mean(out, dim=0)
            ax_mean.imshow(mean.detach().numpy(), cmap=plt.get_cmap('jet'))
            ax_mean.get_xaxis().set_visible(False)
            ax_mean.get_yaxis().set_visible(False)

            std = torch.std(out, dim=0)
            ax_std.imshow(std.detach().numpy(), cmap=plt.get_cmap('jet'))
            ax_std.get_xaxis().set_visible(False)
            ax_std.get_yaxis().set_visible(False)

            if i == 0:
                ax_mean.annotate('Dropout Mean', (-.1, 0.5), xycoords='axes fraction',
                                 va='center', rotation=90)
                ax_std.annotate('Dropout Std. Dev.', (-.1, 0.5), xycoords='axes fraction',
                                va='center', rotation=90)
plt.tight_layout()
plt.savefig("uncertainty.pdf")
plt.show()
