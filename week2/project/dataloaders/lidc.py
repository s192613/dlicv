import torch
import os
import PIL.Image as Image
import torchvision.transforms as transforms
import random


class LIDC(torch.utils.data.Dataset):
    n_annotations = 4

    def __init__(self, fold, root_path="../data/LIDC_crops/LIDC_DLCV_version", mode="first", augmentation=False):
        assert mode in ("first", "all")
        assert fold in ("train", "val", "test")
        self.mode = mode

        self.augmentation = False
        if augmentation and fold == "train":
            self.augmentation = augmentation

        self.base_path = os.path.join(root_path, fold)
        self.images = sorted([f.name for f in os.scandir(os.path.join(self.base_path, 'images/'))])
        self.lesions = sorted([f.name for f in os.scandir(os.path.join(self.base_path, 'lesions/'))])

        self.img_size = 128
        self._trans_dict = {"v_flip": False,
                            "h_flip": False,
                            "rot_angle": 0.0,
                            "crop_height": self.img_size,
                            "crop_width": self.img_size}

    def _transform(self, img):
        img = transforms.functional.center_crop(img, (self._trans_dict["crop_height"], self._trans_dict["crop_width"]))
        img = transforms.functional.resize(img, [self.img_size, self.img_size])
        if self._trans_dict["h_flip"]:
            img = transforms.functional.hflip(img)
        if self._trans_dict["v_flip"]:
            img = transforms.functional.vflip(img)
        img = transforms.functional.rotate(img, self._trans_dict["rot_angle"])
        return transforms.ToTensor()(img)

    def _update_trans_dict(self):
        self._trans_dict["v_flip"] = random.choice([True, False])
        self._trans_dict["h_flip"] = random.choice([True, False])
        self._trans_dict["rot_angle"] = random.uniform(180, -180)
        self._trans_dict["crop_width"] = random.randint(int(self.img_size / 2), self.img_size)
        self._trans_dict["crop_height"] = random.randint(int(self.img_size / 2), self.img_size)

    def __len__(self):
        return 128  # len(self.images)

    def __getitem__(self, idx):
        if self.augmentation:
            self._update_trans_dict()

        image_name = self.images[idx]
        image = Image.open(os.path.join(self.base_path, "images", image_name)).convert('L')

        if self.mode == "first":  # Only the first annotation
            _idx = [self.n_annotations * idx]
        else:  # All the annotations
            _idx = [self.n_annotations * idx + i for i in range(self.n_annotations)]
        lesions = torch.stack([torch.squeeze(
            self._transform(Image.open(os.path.join(self.base_path, "lesions", self.lesions[i])).convert('1'))) for
            i in _idx], dim=0)

        image = self._transform(image)
        return image, lesions
