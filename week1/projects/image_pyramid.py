import imutils
import numpy as np
import cv2


class ImagePyramid:
    def __init__(self, img):
        self.min_size = 32
        self.max_size = 640
        self.scale_factor = 0.5
        self.distortion_factors = [1, 0.75, 0.5, 0.25]
        self.origin_shape = img.shape
        self.img = img

    def _is_in_upper_bound(self, w, h):
        if h > self.max_size or w > self.max_size:
            return False
        return True

    def _is_in_lower_bound(self, w, h):
        if h < self.min_size or w < self.min_size:
            return False
        return True

    def _get_scale(self, w, h):
        return w / self.origin_shape[0], h / self.origin_shape[1]

    def pyramid(self):
        img_size = self.img.shape
        for distortion_factor in self.distortion_factors:
            for i in range(1):
                if distortion_factor == 1 and i > 0:
                    continue
                base_size = list(img_size)
                base_size[i] = int(base_size[i] * distortion_factor)
                resized_base_img = cv2.resize(self.img, (base_size[1], base_size[0]))
                if self._is_in_lower_bound(base_size[0], base_size[1]) and self._is_in_upper_bound(base_size[0],
                                                                                                   base_size[1]):
                    yield self._get_scale(base_size[0], base_size[1]), resized_base_img

                # Downscaling
                w = base_size[0]
                h = base_size[1]
                while True:
                    w = int(w * self.scale_factor)
                    h = int(h * self.scale_factor)
                    if not self._is_in_upper_bound(w, h):
                        continue
                    if not self._is_in_lower_bound(w, h):
                        break
                    temp_img = cv2.resize(resized_base_img, (h, w))
                    yield self._get_scale(w, h), temp_img

                # Upscaling
                w = base_size[0]
                h = base_size[1]
                while True:
                    w = int(w / self.scale_factor)
                    h = int(h / self.scale_factor)
                    if not self._is_in_upper_bound(w, h):
                        break
                    if not self._is_in_lower_bound(w, h):
                        continue
                    temp_img = cv2.resize(resized_base_img, (h, w))
                    yield self._get_scale(w, h), temp_img


if __name__ == '__main__':
    img = np.zeros((500, 50, 3))
    p = ImagePyramid(img)
    for scale, image in p.pyramid():
        print(f"{scale}: {image.shape}")
