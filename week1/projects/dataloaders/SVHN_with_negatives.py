from torch.utils.data import DataLoader, Dataset
import torchvision.transforms as transforms

import torchvision


class SVHN_with_negatives(Dataset):
    def __init__(self, root, train, transform=None):
        """
        Args:
            root (string): Folder, where to save the data.
            train (boolean): True, if training dataset, else svhn_test dataset
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        if train:
            split = "train"
        else:
            split = "test"
        self.img_size = 32
        self.svhn = torchvision.datasets.SVHN("%s/svhn" % root, split=split, download=True, transform=transform)
        self.cifar = torchvision.datasets.CIFAR10("%s/cifar" % root, train=train, transform=transforms.Compose(
            [transforms.RandomCrop(self.img_size), transform]), download=True)
        self.length = len(self.svhn) + int(len(self.svhn) * 2 / 10)

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        if idx < len(self.svhn):
            return self.svhn[idx]
        else:
            image, target = self.cifar[idx - (len(self.svhn) - 1)]
            return (image, 10)

    def get_num_classes(self):
        return 11


if __name__ == '__main__':
    ds = SVHN_with_negatives(root="data/", train=True)
    print(ds[1].shape)