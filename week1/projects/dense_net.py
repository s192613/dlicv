import torch
import torch.nn as nn
import numpy as np


class DenseBlock(nn.Module):
    def __init__(self, num_features, growth_factor, bn=True, dropout_rate=0.0):
        super(DenseBlock, self).__init__()
        if bn:
            self.one_by_one = nn.Sequential(
                nn.BatchNorm2d(num_features=num_features),
                nn.ReLU(),
                nn.Conv2d(num_features, growth_factor, kernel_size=1, stride=1, bias=False),
                nn.Dropout2d(p=dropout_rate)
            )

            self.three_by_three = nn.Sequential(
                nn.BatchNorm2d(num_features=growth_factor),
                nn.ReLU(),
                nn.Conv2d(growth_factor, growth_factor, kernel_size=3, stride=1, padding=1, bias=False),
                nn.Dropout2d(p=dropout_rate)
            )
        else:
            self.one_by_one = nn.Sequential(
                nn.ReLU(),
                nn.Conv2d(num_features, growth_factor, kernel_size=1, stride=1, bias=False),
                nn.Dropout2d(p=dropout_rate)
            )

            self.three_by_three = nn.Sequential(
                nn.ReLU(),
                nn.Conv2d(growth_factor, growth_factor, kernel_size=3, stride=1, padding=1, bias=False),
                nn.Dropout2d(p=dropout_rate)
            )

    def forward(self, x):
        out = self.one_by_one(x)
        out = self.three_by_three(out)
        concat = torch.cat((x, out), 1)
        return concat


class TransitionLayer(nn.Module):
    def __init__(self, num_features, bn=True, dropout_rate=0.0):
        super(TransitionLayer, self).__init__()

        if bn:
            self.one_by_one = nn.Sequential(
                nn.BatchNorm2d(num_features=num_features),
                nn.ReLU(),
                nn.Conv2d(num_features, num_features, kernel_size=1, stride=1, bias=False),
                nn.Dropout2d(p=dropout_rate)
            )
        else:
            self.one_by_one = nn.Sequential(
                nn.ReLU(),
                nn.Conv2d(num_features, num_features, kernel_size=1, stride=1, bias=False),
                nn.Dropout2d(p=dropout_rate)
            )

        self.avg_pool = nn.AvgPool2d(kernel_size=2, stride=2)

    def forward(self, x):
        out = self.avg_pool(self.one_by_one(x))
        return out


class DenseNet(nn.Module):
    def __init__(self, num_classes, blocks=(6, 12, 24, 16), k=12, bn=True, dropout_rate=0.0):
        super(DenseNet, self).__init__()
        # Network definition
        self.conv1 = nn.Conv2d(3, 2 * k, kernel_size=7, stride=2, padding=3)
        self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        # self.conv1b = nn.Conv2d(3, 2 * k, kernel_size=7, stride=2)

        n_blocks = len(blocks)
        ki = 2 * k  # k_{0}
        blocks_list = []
        for l, nb in enumerate(blocks, start=1):
            for n in range(nb):
                blocks_list.append(DenseBlock(ki, k, bn=bn, dropout_rate=dropout_rate))
                ki += k
            if l < n_blocks:  # NOTE: no transition layer after the last block
                blocks_list.append(TransitionLayer(ki, bn=bn, dropout_rate=dropout_rate))
        self.blocks = nn.Sequential(*blocks_list)
        self.global_pool = nn.AvgPool2d(kernel_size=7)
        self.fc = nn.Conv2d(ki, num_classes, kernel_size=1, stride=1)

        # Add initialization
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, np.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.bias.data.zero_()

    def forward(self, x):
        out = self.pool1(self.conv1(x))
        out = self.blocks(out)
        out = self.global_pool(out)
        out = self.fc(out)
        return out
