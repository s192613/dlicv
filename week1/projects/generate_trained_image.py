import week1.projects.helper as helper
import torch
import torch.nn as nn
import torch.nn.functional as F
import tqdm
import matplotlib.pyplot as plt
import numpy as np

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def train(trained_model, img, optimizer, iteration=1000):
    loss_fn = nn.CrossEntropyLoss()
    trained_model.eval()
    for i in tqdm.tqdm(range(iteration), unit='iter'):
        out = trained_model(img.unsqueeze(0))
        loss = - out[:, 0]  # maximize class 0
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

    img_np = img.detach().numpy()
    img_np = np.swapaxes(img_np, 0, 2)
    return img_np, img


if __name__ == '__main__':
    # Load Network
    path = "logs/report/dense-net-v006"
    net, opt = helper.load_checkpoint(path, -1, device, pretrained=True)
    # Random Image
    img = torch.randn((3, 244, 244), requires_grad=True)
    optimizer = helper.get_optimizer(name="adam", net_parameters=[img], lr=0.01,
                                     weight_decay=0.0)

    fig, axs = plt.subplots(nrows=1, ncols=6, figsize=(24, 4))

    # Initial image
    img_np = img.detach().numpy()
    img_np = np.swapaxes(img_np, 0, 2)
    axs[0].imshow(img_np)
    axs[0].set_title('Iteration 0')
    axs[0].set_axis_off()
    j = 1

    net.eval()
    for i in tqdm.tqdm(range(1000), unit='iter'):

        if (i + 1) % 200 == 0:  # plot every 200
            img_np = img.detach().numpy()
            img_np = np.swapaxes(img_np, 0, 2)
            axs[j].set_title(f'Iteration {i+1}')
            axs[j].imshow(img_np)
            axs[j].set_axis_off()
            j += 1
            print(j)

        out = net(img.unsqueeze(0))
        loss = - out[:, 0]  # maximize class 0
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

    fig.savefig("inception.pdf", bbox_inches="tight")
