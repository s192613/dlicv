import numpy as np

def iou(bb1, bb2):
    xc1, yc1, w1, h1 = bb1
    xc2, yc2, w2, h2 = bb2

    # limits of the bb
    x11 = int(xc1 - w1 / 2)
    y11 = int(yc1 - h1 / 2)
    x12 = int(xc1 + w1 / 2)
    y12 = int(yc1 + h1 / 2)

    x21 = int(xc2 - w2 / 2)
    y21 = int(yc2 - h2 / 2)
    x22 = int(xc2 + w2 / 2)
    y22 = int(yc2 + h2 / 2)

    # intersection
    xi1 = max(x11, x21)
    yi1 = max(y11, y21)

    xi2 = min(x12, x22)
    yi2 = min(y12, y22)

    # area of intersection
    ai = max(0, xi2 - xi1 + 1) * max(0, yi2 - yi1 + 1)  # +1 as a pixel is still area

    # individual areas
    a1 = w1 * h1
    a2 = w2 * h2

    return ai / (a1 + a2 - ai)

def non_max_supression(bbs, iou_thresh=0.5):
    final_bbs = []
    for class_i, bbs_per_class in bbs.items():
        while len(bbs_per_class) > 0:
            max_bb = np.argmax(bbs_per_class, axis=0)[0]
            active_bb = bbs_per_class[max_bb]
            del bbs_per_class[max_bb]

            # Find overlapping bounding boxes
            discard_indexes = []
            for i, single_bb in enumerate(bbs_per_class):
                if iou(active_bb[1:5], single_bb[1:5]) >= iou_thresh:
                    discard_indexes.append(i)

            # Discard overlapping bounding boxes
            discard_indexes.sort(reverse=True)
            for i in discard_indexes:
                del bbs_per_class[i]

            # save active bounding box as final one
            active_bb.insert(0, class_i)
            final_bbs.append(active_bb)
    return final_bbs