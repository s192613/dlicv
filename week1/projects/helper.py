import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
import numpy as np
import random
import os
import json

# TODO: you might need to change the path to those files
from week1.projects.dataloaders.Hotdog_NotHotdog import Hotdog_NotHotdog
from week1.projects.dataloaders.SVHN_with_negatives import SVHN_with_negatives
from week1.projects.dense_net import DenseNet
from week1.projects.dense_net2 import DenseNet2

# Expected size for the DenseNet
input_size = {"hotdog": 224,
              "svhn": 32}

# Statistics for normalization
norm_mean = [0.485, 0.456, 0.406]
norm_std = [0.229, 0.224, 0.225]

default_normalise = transforms.Normalize(mean=norm_mean,  # same normalization as pre-trained models
                                         std=norm_std)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def generate_augmentation_dict(vertical_flip, horizontal_flip, random_erase, random_rotation, random_crop_from,
                               dataset_id):
    size = input_size[dataset_id]
    augmentation = {}
    augmentation["vertical_flip"] = vertical_flip
    augmentation["horizontal_flip"] = horizontal_flip
    augmentation["random_erase"] = random_erase
    if random_crop_from >= size:
        augmentation["random_crop_from"] = random_crop_from
    if random_rotation > 0:
        augmentation["random_rotation"] = random_rotation
    return augmentation


def generate_test_transform(augmentation, dataset_id):
    size = input_size[dataset_id]
    transform_list = []
    if "random_crop_from" not in augmentation:
        transform_list.append(transforms.Resize((size, size)))
    else:
        transform_list.append(transforms.Resize(size))
        transform_list.append(transforms.CenterCrop(size))

    transform_list.append(transforms.ToTensor())
    transform_list.append(default_normalise)
    return transforms.Compose(transform_list)


def generate_train_transform(augmentation, dataset_id):
    size = input_size[dataset_id]
    transform_list = []
    if "random_crop_from" not in augmentation:
        transform_list.append(transforms.Resize((size, size)))
    else:
        transform_list.append(transforms.Resize(augmentation["random_crop_from"]))
        transform_list.append(transforms.RandomCrop((size, size)))

    if "random_rotation" in augmentation:
        transform_list.append(transforms.RandomRotation(augmentation["random_rotation"]))
    if "vertical_flip" in augmentation and augmentation["vertical_flip"]:
        transform_list.append(transforms.RandomVerticalFlip())
    if "horizontal_flip" in augmentation and augmentation["horizontal_flip"]:
        transform_list.append(transforms.RandomHorizontalFlip())

    transform_list.append(transforms.ToTensor())

    if "random_erase" in augmentation and augmentation["random_erase"]:
        transform_list.append(transforms.RandomErasing())

    transform_list.append(default_normalise)
    return transforms.Compose(transform_list)


def prepare_data(data_path, batch_size=64, augmentation=None, dataset_id="hotdog"):
    train_transform = generate_train_transform(augmentation, dataset_id)
    test_transform = generate_test_transform(augmentation, dataset_id)

    if dataset_id == "hotdog":
        train_set = Hotdog_NotHotdog(True, train_transform, data_path=f"{data_path}/hotdog_nothotdog")
        test_set = Hotdog_NotHotdog(False, test_transform, data_path=f"{data_path}/hotdog_nothotdog")
    elif dataset_id == "svhn":
        train_set = SVHN_with_negatives(data_path, True, train_transform)
        test_set = SVHN_with_negatives(data_path, False, test_transform)
    else:
        raise NotImplementedError()

    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=1)
    test_loader = DataLoader(test_set, batch_size=batch_size, shuffle=True, num_workers=1)

    return (train_set, train_loader), (test_set, test_loader)


def get_optimizer(name, net_parameters, lr, weight_decay):
    if name.lower() == 'sgd':
        return torch.optim.SGD(net_parameters, lr, weight_decay=weight_decay, momentum=0.9)
    elif name.lower() == 'adam':
        return torch.optim.Adam(net_parameters, lr, weight_decay=weight_decay)
    elif name.lower() == "rmsprop":
        return torch.optim.RMSprop(net_parameters, lr, weight_decay=weight_decay)
    else:
        raise NotImplementedError()


def set_seeds(seed=42):
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)


def get_version(name="dense-net", logdir='logs/', width=3):
    os.makedirs(logdir, exist_ok=True)
    files = list(sorted([f for f in os.listdir(logdir) if f"{name}-v" in f]))
    if len(files) < 1:
        version = '1'.rjust(width, '0')
    else:
        last_version = int(files[-1][-width:])
        version = str(last_version + 1).rjust(width, '0')
    return version


def save_config(path, args):
    with open(os.path.join(path, "config.json"), "w") as json_f:
        json.dump(vars(args), json_f)


def read_config(path):
    with open(os.path.join(path, "config.json")) as json_f:
        return json.load(json_f)


def load_checkpoint(log_dir, epoch, device, pretrained=False):
    if epoch == -1:
        epoch = max(map(lambda s: int(s[0:-4].split('_')[1]), [f for f in os.listdir(log_dir) if "checkpoint" in f]))
    config = read_config(log_dir)
    fname = f"checkpoint_{epoch}.tar"
    path = os.path.join(log_dir, fname)
    checkpoint = torch.load(path, map_location=device)
    if pretrained:
        net = torch.hub.load('pytorch/vision:v0.6.0', 'densenet121', pretrained=True)
    else:
        if config["dataset_id"] == "hotdog":
            net = DenseNet(k=config['k'], bn=config['bn'], dropout_rate=config['dropout_rate'],
                           num_classes=config["num_classes"])
        elif config["dataset_id"] == "svhn":
            net = DenseNet2(k=config['k'], bn=config['bn'], dropout_rate=config['dropout_rate'],
                            num_classes=config["num_classes"])

    net.load_state_dict(checkpoint["net_state_dict"])
    opt = get_optimizer(config["optimizer"], net.parameters(), config["lr"], config["weight_decay"])
    opt.load_state_dict(checkpoint["opt_state_dict"])
    return net, opt
