import torch
import os
import week1.projects.helper as helper
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

if __name__ == '__main__':
    plt.style.use('ggplot')
    model_path = "logs/report/number_detector"
    img_path = "data/svhn_test"

    config = helper.read_config(model_path)
    net, opt = helper.load_checkpoint(model_path, -1, device, pretrained=False)
    net.eval()

    augmentation = helper.generate_augmentation_dict(config["aug_randomvertflip"],
                                                     config["aug_randomhorizflip"],
                                                     config["aug_randomerase"],
                                                     config["aug_randomrot"],
                                                     config["aug_randomcrop"],
                                                     config["dataset_id"])

    (_, _), (test_set, test_loader) = helper.prepare_data("data", 64, augmentation, dataset_id="svhn")

    confusion_matrix = np.zeros((11, 11), dtype=int)

    for i, batch in enumerate(test_loader):
        imgs, labels = batch
        pred = torch.squeeze(net(imgs)).argmax(1)
        for true, pred in zip(labels, pred):
            confusion_matrix[true, pred] += 1

        if i > 200:
            break
    row_sums = confusion_matrix.sum(axis=1)
    confusion_matrix = confusion_matrix / row_sums[:, np.newaxis]

    sns.heatmap(confusion_matrix, annot=True, fmt=".2f", cbar=False, square=True)
    plt.tight_layout()
    plt.savefig(os.path.join(model_path, "confusion.pdf"))
