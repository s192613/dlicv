import argparse
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from cycler import cycler
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator

default_cycler = cycler(color=mpl.colors.TABLEAU_COLORS)

plt.style.use('ggplot')
plt.rc('axes', prop_cycle=default_cycler)

PLOT_CONTEXT = {
    'text.usetex': True,
    'font.size': 10,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
    'axes.formatter.useoffset': False,
    "text.latex.preamble": [r'\usepackage{cmbright}']
}

M = ['loss', 'accuracy', 'dice', 'iou', 'sensitivity', 'specificity', ]
F = ['train', 'val', 'test']
TB_PLOTS = {m: {"xlabel": "epoch",
                "ylabel": m,
                "tb_fields": {f"{m}/{f}": f"{f} {m}" for f in F}} for m in M}


def parse_args():
    parser = argparse.ArgumentParser(description="script making plots.")
    parser.add_argument('-m', '--mode', type=str, required=True, choices=('single', 'multiple'), default='single')
    parser.add_argument('-l', '--logdir', type=str, required=True,
                        help="path to the directory containing the necessary files for plotting")
    return parser.parse_args()


def plot_tb_logs(event_accs, plots_path):
    with mpl.rc_context(PLOT_CONTEXT):
        for pname, pdict in TB_PLOTS.items():
            if len(event_accs) > 1:
                ncols = len(pdict["tb_fields"])
                legends = [f"model-{i + 1}" for i in range(len(event_accs))]
                titles = list(pdict["tb_fields"].values())
            else:
                ncols = 1
                legends = list(pdict["tb_fields"].values())
                titles = [pname]

            f, axs = plt.subplots(ncols=ncols, figsize=(ncols * 5, 4), sharey=True)

            if ncols == 1:
                axs = [axs]
            for i, ax in enumerate(axs):
                ax.set_xlabel(pdict['xlabel'])
                ax.set_ylabel(pdict['ylabel'])
                ax.set_title(titles[i])

            for i, field in enumerate(pdict["tb_fields"]):
                axi = i % ncols
                for event_acc in event_accs:
                    try:
                        _, step, val = zip(*event_acc.Scalars(field))
                        axs[axi].plot(step, val)
                    except KeyError:
                        print(f"Could not plot {field}, in {pname}")
                        continue

            f.legend(legends, loc=7)
            f.tight_layout()
            f.subplots_adjust(right=0.87)
            f.savefig(f"{plots_path}/{pname}.pdf")
            plt.close(f)


def main_single(plots_path, args):
    event_acc = EventAccumulator(args.logdir)
    event_acc.Reload()
    plot_tb_logs(event_accs=[event_acc], plots_path=plots_path)


def main_multiple(plots_path, args):
    dirs = list(sorted([os.path.join(args.logdir, d) for d in os.listdir(args.logdir) if
                        "plots" not in d and os.path.isdir(os.path.join(args.logdir, d))]))
    event_accs = []

    for dir in dirs:
        event_acc = EventAccumulator(dir)
        event_acc.Reload()
        event_accs.append(event_acc)

    plot_tb_logs(event_accs=event_accs, plots_path=plots_path)


if __name__ == '__main__':
    args = parse_args()

    plots_path = os.path.join(args.logdir, "plots")
    os.makedirs(plots_path, exist_ok=True)

    if args.mode == "single":
        main_single(plots_path, args)
    elif args.mode == "multiple":
        main_multiple(plots_path, args)
    else:
        raise NotImplementedError()
