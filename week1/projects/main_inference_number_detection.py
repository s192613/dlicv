from week1.projects.image_pyramid import ImagePyramid
import torch
import week1.projects.helper as helper
import glob
from PIL import Image, ImageDraw, ImageColor
import numpy as np
import torchvision.transforms as transforms
import torch.nn.functional as F
import matplotlib.pyplot as plt
from week1.projects.non_max_suppression import non_max_supression
import random
import os

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
p_c = 0.98
iou_thresh = 0.2

def infer(model, data):
    data = transforms.ToTensor()(data)
    data = helper.default_normalise(data)
    data = data.unsqueeze(0)
    data = data.to(device)
    with torch.no_grad():
        output = model(data)
    return output.cpu()

def extract_bounding_boxes(tensor, img, scale, bbs):
    base_img_size = 32
    num_class, t_w, t_h = tensor.shape
    i_w, i_h, c = img.shape

    for i in range(t_w):
        for j in range(t_h):
            class_vec = F.softmax(tensor[:, i, j], dim=0)
            bb_c = class_vec.argmax().item()
            bb_p = max(class_vec).item()
            if bb_c >= 10:
                continue
            if bb_p <= p_c:
                continue
            bb_x = (i_w/t_w/2 + i * i_w/t_w)/scale[0]
            bb_y = (i_h/t_h/2 + j * i_h/t_h)/scale[1]
            bb_w = base_img_size/scale[0]
            bb_h = base_img_size/scale[1]
            bbs[bb_c].append([bb_p, bb_x, bb_y, bb_w, bb_h])
    return bbs

def draw_bb_on_img(img, bbs):
    colors = [ImageColor.getrgb("white"),
              ImageColor.getrgb("grey"),
              ImageColor.getrgb("yellow"),
              ImageColor.getrgb("orange"),
              ImageColor.getrgb("red"),
              ImageColor.getrgb("purple"),
              ImageColor.getrgb("blue"),
              ImageColor.getrgb("cyan"),
              ImageColor.getrgb("green"),
              ImageColor.getrgb("black")]
    im = Image.fromarray(img)
    draw = ImageDraw.Draw(im)
    for bb in bbs:
        x1 = int(bb[2] - bb[4] / 2)
        y1 = int(bb[3] - bb[5] / 2)
        x2 = int(bb[2] + bb[4] / 2)
        y2 = int(bb[3] + bb[5] / 2)
        draw.rectangle([y1, x1, y2, x2], outline=colors[bb[0]])
    del draw
    return im


if __name__ == '__main__':
    model_path = "logs/dense-net-v009"
    img_path = "../../data/svhn_full_test"
    save_path = f"{model_path}/{p_c}_{iou_thresh}"

    os.makedirs(save_path, exist_ok=True)


    net, opt = helper.load_checkpoint(model_path, 9, device, pretrained=False)
    net.eval()

    img_files = glob.glob(f"{img_path}/*.png")

    for j, img_file in enumerate(img_files[1:50]):
        bbs = {0: [], 1: [], 2: [], 3: [], 4:[], 5:[], 6:[], 7:[], 8:[], 9:[]}
        pil_img = Image.open(img_file)
        img = np.asarray(pil_img)
        ip = ImagePyramid(img)
        for scale, i_img in ip.pyramid():
            out = infer(net, i_img)
            bbs = extract_bounding_boxes(out.squeeze(0), i_img, scale, bbs)
        bbs = non_max_supression(bbs, iou_thresh=iou_thresh)
        # ToDo NonMaxSuppression(bbs)
        img_p = draw_bb_on_img(img, bbs)
        plt.imshow(img_p)
        plt.show()
        img_p.save(f"{save_path}/{j}.jpg")


