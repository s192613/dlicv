import os
import argparse
import torch
import torch.nn as nn
import tqdm
from torch.utils.tensorboard import SummaryWriter
import numpy as np

import week1.projects.helper as helper
from week1.projects.dense_net import DenseNet
from week1.projects.dense_net2 import DenseNet2

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def parse_cmd():
    parser = argparse.ArgumentParser(description="script to train DenseNet model for the HotDog/NotHotDog task.")
    parser.add_argument('--model_name', type=str, default='dense-net',
                        help='name given to the mode (default: dense-net')
    parser.add_argument('--batch_size', type=int, default=32, help='input batch size for training (default: 32)')
    parser.add_argument('--dataset_id', type=str, default="svhn", choices=('hotdog', 'svhn'), help='The training dataset (default: hotdog)')
    parser.add_argument('--num_epochs', type=int, default=50, help='number of training epochs (default: 50)')
    parser.add_argument('--k', type=int, default=12,
                        help='growth rate of the DenseNet(default: 12)')
    parser.add_argument('--aug_randomhorizflip', type=bool, default=False,
                        help='Appplying random horizonal flipping (default: False)')
    parser.add_argument('--aug_randomvertflip', type=bool, default=False,
                        help='Appplying random vertical flipping (default: False)')
    parser.add_argument('--aug_randomerase', type=bool, default=False, help='Appplying random erasing (default: False)')
    parser.add_argument('--aug_randomrot', type=int, default=0,
                        help='Amount of degree to apply random rotation (default: 0)')
    parser.add_argument('--aug_randomcrop', type=int, default=0,
                        help='Base image size from that will be randomly cropped. Must be > 224. (default: 0)')
    parser.add_argument('--optimizer', type=str, default='sgd', choices=('sgd', 'adam', 'rmsprop'),
                        help='optimizer to be used (default:sgd)')
    parser.add_argument('--lr', type=float, default=1e-3, help='learning rate (default: 1e-2)')
    parser.add_argument('--lr_step', type=int, default=-1, help='learning rate decrease step (default: -1)')
    parser.add_argument('--weight_decay', type=float, default=.0, help='weight decay, L2 penalty (default: 0.0)')
    parser.add_argument('--bn', type=bool, default=True, help='to apply BN in the network or not (default: False)')
    parser.add_argument('--dropout_rate', type=float, default=.0,
                        help='dropout to be applied in the network (default: 0.0)')
    parser.add_argument('--logdir', type=str, default='logs',
                        help='directory where the logs will be saved (default: logs/)')
    parser.add_argument('--save_every', type=int, default=5,
                        help='frequency at which the network should be saved (default: 5)')
    parser.add_argument('--data_path', type=str, default='data',
                        help='location of the dataset (default: data)')
    parser.add_argument('--pretrained', type=bool, default=False,
                        help='Using pretrained Network')

    args = parser.parse_args()

    return args


def train(model, train_set, train_loader, test_set, test_loader, optimizer, scheduler, writer, log_path, num_epochs, save_every):
    model.to(device)
    # ToDo: add condition to only apply on svhn
    # w_arr = [1, 0.2, 1, 1, 1, 1, 1, 1, 1, 1, 2]
    # class_weights = torch.Tensor(w_arr).to(device)
    loss_fun = nn.CrossEntropyLoss()
    for epoch in tqdm.tqdm(range(num_epochs), unit='epoch'):
        print(f"------------- EPOCH {epoch}/{num_epochs} ---------------")
        model.train()
        # For each epoch
        train_correct = 0
        train_loss = []
        for minibatch_no, (data, target) in tqdm.tqdm(enumerate(train_loader), total=len(train_loader)):
            data, target = data.to(device), target.to(device)
            # Zero the gradients computed for each weight
            optimizer.zero_grad()
            # Forward pass your image through the network
            output = torch.squeeze(model(data))
            # Compute the loss
            loss = loss_fun(output, target)
            train_loss.append(loss.cpu().item())
            # Backward pass through the network
            loss.backward()
            # Update the weights
            optimizer.step()

            # Compute how many were correctly classified
            predicted = output.argmax(1)
            correct = (target == predicted).sum().cpu().item()
            train_correct += correct

        # Compute the svhn_test accuracy
        test_loss = []
        test_correct = 0
        model.eval()
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            with torch.no_grad():
                output =  torch.squeeze(model(data))
            test_loss.append(loss_fun(output, target).cpu().item())
            predicted = output.argmax(1)
            test_correct += (target == predicted).sum().cpu().item()
        train_accuracy = train_correct / len(train_set)
        test_accuracy = test_correct / len(test_set)
        print(f"Train Accuracy: {train_accuracy:.2f} \t Test Accuracy: {test_accuracy:.2f}")
        # Logging
        writer.add_scalar('accuracy/train', train_accuracy, global_step=epoch)
        writer.add_scalar('accuracy/svhn_test', test_accuracy, global_step=epoch)
        writer.add_scalar('loss/train', np.mean(train_loss), global_step=epoch)
        writer.add_scalar('loss/svhn_test', np.mean(test_loss), global_step=epoch)

        # Save
        if (epoch + 1) % save_every == 0:
            torch.save({"net_state_dict": net.state_dict(),
                        "opt_state_dict": optimizer.state_dict()},
                       os.path.join(log_path, f"checkpoint_{epoch}.tar"))
        # scheduler step
        scheduler.step()


if __name__ == '__main__':
    # helper.set_seeds()  # reproducibility
    args = parse_cmd()
    # Get the data
    augmentation = helper.generate_augmentation_dict(args.aug_randomvertflip,
                                                     args.aug_randomhorizflip,
                                                     args.aug_randomerase,
                                                     args.aug_randomrot,
                                                     args.aug_randomcrop,
                                                     args.dataset_id)

    (train_set, train_loader), (test_set, test_loader) = helper.prepare_data(args.data_path, args.batch_size,
                                                                             augmentation, dataset_id= args.dataset_id)

    num_classes = train_set.get_num_classes()
    args.num_classes = num_classes
    # Init the network
    if args.pretrained:
        net = torch.hub.load('pytorch/vision:v0.6.0', 'densenet121', pretrained=True)
    else:
        if args.dataset_id == "hotdog":
            net = DenseNet(num_classes, k=args.k, bn=args.bn, dropout_rate=args.dropout_rate, mode=args.dataset_id)
        elif args.dataset_id == "svhn":
            net = DenseNet2(num_classes, k=args.k, bn=args.bn, dropout_rate=args.dropout_rate)

    optimizer = helper.get_optimizer(name=args.optimizer, net_parameters=net.parameters(), lr=args.lr,
                                     weight_decay=args.weight_decay)

    scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                step_size=args.num_epochs if args.lr_step < 0 else args.lr_step,
                                                gamma=.1)

    # Logging
    net_version = helper.get_version(name=args.model_name, logdir=args.logdir)
    log_path = os.path.join(args.logdir, args.model_name + f"-v{net_version}")
    writer = SummaryWriter(log_path)
    helper.save_config(log_path, args)  # save to json the current configuration

    # train the network
    train(net, train_set, train_loader, test_set, test_loader, optimizer, scheduler, writer, log_path, args.num_epochs,
          args.save_every)
